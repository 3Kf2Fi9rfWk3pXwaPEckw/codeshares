import argparse
import cartopy.crs as ccrs
import logging
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.collections as clls
import numpy as np
import os
import pandas as pd

from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER


def get_stream_data(paths):

    all_streams = None
    i = 0
    for path in paths:
        logger.debug('\n' + path)
        streams = pd.read_csv(path, delim_whitespace=True,
                              names=['Label', 'Long', 'Lat', 'Wind Speed',
                                     'Angle'])
        if all_streams is None:
            all_streams = streams
        else:
            all_streams = pd.concat([all_streams, streams])
        i += 1
        logger.debug('File number: {0}'.format(i))
    new_streams = all_streams['Label'] == 0
    logger.debug('\nTotal number of streams: {0}'.format(sum(new_streams)))
    all_streams['Stream Label'] = np.cumsum(new_streams)
    return all_streams


def plot(streams, args):

    logger.info("\nPlotting streams.")
    ax = prepare_figure()

    if args.percentile is None:
        all_line_segs, all_avg_speeds = [], []

        for group, stream in streams.groupby('Stream Label'):
            wind_speeds = stream['Wind Speed']
            longs = stream['Long']
            lats = stream['Lat']
            # Column vector of wind speeds
            avg_wind_speeds = (wind_speeds + np.roll(wind_speeds, 1)) / 2.
            # Find line segments. line_seg_points is a 3D array,
            # cross-sectional area = 4 units, and depth equal to the total
            # number of line segments in the stream
            lon_lat = np.dstack([longs, lats]).reshape(-1, 1, 2)
            logger.debug('\nLon-lat: \n{1}\n{0}'.format(lon_lat, type(lon_lat)))
            line_seg_points = np.concatenate((lon_lat, 
                                              np.roll(lon_lat, 1, axis=0)),
                                             axis=1)
            # Appending these arrays to lists so that, before plotting, can
            # extract maximum and minimum values for coloring purposes
            all_avg_speeds.append(avg_wind_speeds) 
            all_line_segs.append(line_seg_points)

        vmin = max((min([np.min(speeds) for speeds in all_avg_speeds]),
                    args.min))
        vmax = max([np.max(speeds) for speeds in all_avg_speeds])
        color_bar_label = 'Wind speeds (bounding node averages) (m/s)'
        line_s_m = make_colorbar(vmin, vmax, color_bar_label, 
                                 matplotlib.cm.rainbow)
        # Make all values below minimum threshold transparent (i.e. invisible)
        line_s_m.get_cmap().set_under(alpha=0)

        i = 0
        for speeds, line_seg_points in zip(all_avg_speeds, all_line_segs):
            i += 1
            speeds_above_threshold = np.array(speeds > args.min, dtype=np.bool)
            # Ignore data with wind speed below specified min threshold value
            speeds = speeds[speeds_above_threshold]
            line_seg_points = line_seg_points[speeds_above_threshold, ...]
            colours = line_s_m.to_rgba(speeds)
            line_segments = clls.LineCollection(line_seg_points, linewidths=2,
                                                colors=colours)
            logger.debug('\nPlotting stream {0}'.format(i))
            line_segments.set_transform(ccrs.PlateCarree())
            ax.add_collection(line_segments)

    else:
        if not 0 <= args.percentile <= 100:
            raise ValueError('Please choose a percentile between 0 and 100.')
        # Copy of streams but without data below minimum wind speeds threshold
        TEMP_streams = streams[streams['Wind Speed'] > args.min]
        # Map group label to color
        line_color_values = \
            {group: np.percentile(stream['Wind Speed'], args.percentile) for
             group, stream in TEMP_streams.groupby('Stream Label')}
        
        color_bar_label = 'Wind speed ({0}th percentile of stream ' \
                          'range) (m/s)'.format(args.percentile)
        line_s_m = make_colorbar(min(line_color_values.values()),
                                 max(line_color_values.values()),
                                 color_bar_label,
                                 matplotlib.cm.cool)

        for group, stream in streams.groupby('Stream Label'):
            # Ignore stream if no group: color map in line_color_values
            # i.e. if all points on the stream have wind speed < minimum
            # threshold value
            color = line_color_values.get(group)
            if color is None:
                continue
            longs, lats = stream['Long'], stream['Lat']
            lon_lat = np.dstack([longs, lats]).reshape(-1, 1, 2)
            line_seg_points = np.concatenate((lon_lat, 
                                              np.roll(lon_lat, 1, axis=0)),
                                             axis=1)
            wind_speeds = np.array(stream['Wind Speed'])
            line_seg_points = line_seg_points[wind_speeds > args.min]
            line_segments = clls.LineCollection(line_seg_points, linewidths=2,
                                                colors=line_s_m.to_rgba(color))
            logger.debug('\nPlotting stream {0}'.format(group))
            line_segments.set_transform(ccrs.PlateCarree())
            ax.add_collection(line_segments)

    plt.title(args.title, y=1.2)
    if args.output is not None:
        plt.savefig(os.path.abspath(args.output))
    else:
        plt.show()


def prepare_figure():

    ax = plt.gca(projection=ccrs.PlateCarree(central_longitude=180))
    ax.coastlines()
    ax.set_global()
    gl = ax.gridlines()
    gl.yformatter = LATITUDE_FORMATTER
    gl.xformatter = LONGITUDE_FORMATTER
    return ax


def make_colorbar(minimum, maximum, title, cmap):

    norm = matplotlib.colors.Normalize(vmin=minimum, vmax=maximum)
    color_scheme = cmap
    s_m = matplotlib.cm.ScalarMappable(cmap=color_scheme, norm=norm)
    s_m.set_array([])
    bar = plt.colorbar(s_m, shrink=0.7)
    bar.set_label(title)
    return s_m


if __name__ == '__main__':

    logger = logging.getLogger('jet_streams')
    logger.addHandler(logging.StreamHandler())

    desc_str = """Visualise jet stream data using csv in the format: \n 
    <Point number> <Longitude> <Latitude> <Wind Speed> <Azimuthal angle>."""

    parser = argparse.ArgumentParser(description=desc_str, 
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('path', nargs='+',
                        help='Path(s) to data-hosting file(s).')
    parser.add_argument('--title', help='Figure title.')
    parser.add_argument('--min', '-m', type=float, default=40.,
                        help='Minimum wind speed for rendering [m/s].')
    parser.add_argument('--percentile', '-p', default=None,
                        help="""Color streams according to the nth percentile
                                of the individual jet streams' wind speed
                                distributions, where n = value given here.""")
    parser.add_argument('--output', '-o', help='Destination of output image.')
    quiet_or_verbose = parser.add_mutually_exclusive_group()
    quiet_or_verbose.add_argument('--verbose', '-v', dest='log_level',
                                  help='Turn on verbose output.',
                                  action='store_const', const=logging.DEBUG)
    quiet_or_verbose.add_argument('--quiet', '-q', dest='log_level',
                                  help="""Prevent any output, other than that of
                                          the script being executed.""",
                                  action='store_const', const=logging.ERROR)
    args = parser.parse_args()

    if args.percentile is not None:
        args.percentile = float(args.percentile)

    if args.log_level is not None:
        logger.setLevel(args.log_level or logging.INFO)

    np.set_printoptions(threshold=np.inf)
    
    logger.debug("\nargs: \n{0}".format(args))
    streams = get_stream_data(args.path)
    plot(streams, args)
