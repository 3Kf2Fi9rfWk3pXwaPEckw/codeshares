"""
Sample for running the GungHo 'library'.

Creates (loads) a set of verts and faces from the verts and visualises these
on a matplotlib plot.

NOTES:
    - the plotting currently evaluates each cell discretely using a subgrid.
    - there is tight coupling between DoFs, nodes and cells.
    - there is tight coupling between cells and reference cells (on number of
      coefficients), and no generalised `solve` function - perhaps `solve`
      should belong to the cell class?

TODO:
    - Improve (decouple) cells from verts; cells and verts from DoFs (i.e.
      create a DoFmap). This will require a new loader in `gungho`.
    - evaluate the mesh.

"""


from collections import namedtuple
import numpy as np

import gungho


element = namedtuple('element', ('inds', 'coefficient', 'x', 'y'))


def main():
    # DoF array.
    dofs = np.arange(1, 37)

    # Vertices. Repeated verts within the mesh are commented out.
    verts = {'a': element(dofs[0], 1.25, 1, 1),
             'b': element(dofs[4], 2.5, 2, 1),
             'c': element(dofs[16], 1.4, 3, 1),
             # 'd': element(dofs[0], 2, 4, 1),
             'e': element(dofs[12], 0.2, 1, 2),
             'f': element(dofs[8], 0.25, 2, 2),
             'g': element(dofs[20], 0.95, 3, 2),
             # 'h': element(dofs[12], 0.75, 4, 2),
             'i': element(dofs[24], 0.53, 1, 3),
             'j': element(dofs[28], 1.34, 2, 3),
             'k': element(dofs[32], 0.28, 3, 3),
             # 'l': element(dofs[24], 2.8, 4, 3),
             # 'm': element(dofs[0], 1.5, 1, 4),
             # 'n': element(dofs[4], 2.32, 2, 4),
             # 'o': element(dofs[16], 2.1, 3, 4),
             # 'p': element(dofs[0], 1.61, 4, 4),
             }

    # Cells. Cells share verts. 
    cells = {1: element(['a', 'b', 'f', 'e'], 0.67, None, None),
             2: element(['b', 'c', 'g', 'f'], 0.96, None, None),
             3: element(['c', 'a', 'e', 'g'], 3.51, None, None),
             4: element(['e', 'f', 'j', 'i'], 1.22, None, None),
             5: element(['f', 'g', 'k', 'j'], 2.45, None, None),
             6: element(['g', 'e', 'i', 'k'], 1.67, None, None),
             7: element(['i', 'j', 'b', 'a'], 1.28, None, None),
             8: element(['j', 'k', 'c', 'b'], 2.67, None, None),
             9: element(['k', 'i', 'a', 'c'], 0.74, None, None),
             }

    _, f = gungho.load(dofs, verts, vert_face_mapping=cells)
    gungho.visualise(f, key='combined')


if __name__ == '__main__':
    main()
