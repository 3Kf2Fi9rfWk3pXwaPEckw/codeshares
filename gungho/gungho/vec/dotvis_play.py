"""
A playground for producing graphs with dot.

See: https://pythonhaven.wordpress.com/2009/12/09/generating_graphs_with_pydot/
     https://pypi.python.org/pypi/pydot

"""

import pydot
from PIL import Image

# Make a graph - of type _di_rected _graph_.
graph = pydot.Dot(graph_type='digraph')

# And define a filename to save the graph as.
img_name = 'my_first_graph.png'

##############################################################################
# Let's make a graph that looks like my sample topology.                     #
# This has four verts V1 - V4 joined by four edges E7, E13, E11, E14.        #
# The result of this we call C1.                                             #
#                                                                            #
# NOTE: Technically the edges have a forward direction. This can be added if #
# simple or ignored if not...                                                #
##############################################################################

# Create nodes to add to the graph.
node_v1 = pydot.Node("V1")
node_v2 = pydot.Node("V2")
node_v3 = pydot.Node("V3")
node_v4 = pydot.Node("V4")

# Add nodes to the graph.
graph.add_node(node_v1)
graph.add_node(node_v2)
graph.add_node(node_v3)
graph.add_node(node_v4)

# Create edges between nodes.
edge_e7 = pydot.Edge(node_v1, node_v4, label='E7')
edge_e11 = pydot.Edge(node_v3, node_v2, label='E11')
edge_e13 = pydot.Edge(node_v4, node_v3, label='E13')
edge_e14 = pydot.Edge(node_v2, node_v1, label='E14')

# Add edges to the graph too.
graph.add_edge(edge_e7)
graph.add_edge(edge_e11)
graph.add_edge(edge_e13)
graph.add_edge(edge_e14)

# Create the graph image and then show it.
graph.write_png(img_name)
graph_img = Image.open(img_name)
graph_img.show()

