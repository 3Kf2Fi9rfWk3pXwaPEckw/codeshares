"""
VEC: node topology
==================

Introduction
------------

A Python-based way of describing the elements of topology: we have Vertices
(points; aka Verts because shorter), Edges (sets of two points that define an
edge when connected), and Cells (2D collections of three or more Vertices or
Edges).

Let's imagine that Verts, Edges and Cells are all related via a sort of
database structure. As Verts are the building blocks of edges and cells (via
one or two steps) it makes sense to have the identity table constructed from
the set of Verts in our system.
We can then define tables of Edges, Cells (either as sets of Verts or Edges)
and Vert data that use the Verts identity table for foreign keys.


Nomenclature
------------

 * Vertex: vert, point.
 * Edge: line.
 * Cell: face, volume, (polygon).


Why VEC?
--------

The name comes from the names for each element of topology: we have Vertices
(points; aka Verts because shorter), Edges (sets of two points that define an
edge when connected), and Cells (2D collections of three or more Vertices or
Edges).

"""


class Vertex(object):
    """
    A Vertex is a point; the fundamental topology element.

    Do verts need data?

    """
    def __init__(self, index, data=None):
        self.index = index
        self.data = data

        self._interpret_data()

    def __str__(self):
        return self.name()

    def name(self):
        return 'V{}'.format(self.index)

    def _interpret_data(self):
        # This needs to add an attribute to the class for each k/v pair in the
        # incoming data dictionary if data is a dictionary.
        if self.data is not None:
            if isinstance(self.data, dict):
                for k, v in self.data.iteritems():
                    setattr(self, k, v)


class Edge(object):
    """
    An Edge is a collection of two Vertices.

    Edges need a forward and reverse.

    """
    def __init__(self, index, verts, front=None):
        self.index = index
        self._verts = verts
        self.front = front

        self._forward = None
        self._reverse = None

    def __str__(self):
        return self.name()

    def name(self):
        return 'E{}'.format(self.index)

    @property
    def verts(self):
        if self.front:
            verts = self._verts
        else:
            verts = self._verts[::-1]
        return verts

    @property
    def forward(self):
        if self.front is not None:
            self._forward = self.verts[0]
        return self._forward

    @property
    def reverse(self):
        if self.front is not None:
            self._reverse = self.verts[1]
        return self._reverse


class Face(object):
    """
    A Face is a 2D topology element. It must contain three or more Edges or
    Vertices.

    Do cells need to close?

    """
    def __init__(self, index, elements, ordered=None, data=None):
        self.index = index
        self._elements = elements
        self.ordered = ordered
        self.data = data

        # Check elements is long enough to make a cell.
        self._verify()
        self._interpret_data()

    def _verify(self):
        """A cell needs to be made of at least 3 input elements."""
        valid = len(self.elements) > 2
        if not valid:
            msg = "Not enough elements to make a valid cell."
            raise ValueError(msg)

    def _interpret_data(self):
        # This needs to add an attribute to the class for each k/v pair in the
        # incoming data dictionary if data is a dictionary.
        if self.data is not None:
            if isinstance(self.data, dict):
                for k, v in self.data.iteritems():
                    setattr(self, k, v)

    def __repr__(self):
        contents = {}
        if hasattr(self.elements[0], 'verts'):
            # We have a cell of edges.
            contents['edges'] = [element.name() for element in self.elements]
            contents['verts'] = [e.verts[0].name() for e in self.elements]
        else:
            # We have a cell of verts.
            contents['verts'] = [element.name() for element in self.elements]
        return 'Cell {}; {}'.format(self.name, contents)

    def __str__(self):
        return self.name()

    def name(self):
        return 'F{}'.format(self.index)

    @property
    def elements(self):
        """
        If verts have an order then they need to retain that order.
        Otherwise we can have them in any specified order.

        """
        return self._elements

    def contents(self):
        """
        Determine the contents that have been used to create the cell.

        """
        result = {}
        if hasattr(self.elements[0], 'verts'):
            # We have a cell of edges.
            result['edges'] = list(self.elements)
            result['verts'] = [e.verts[0] for e in self.elements]
        else:
            # We have a cell of verts.
            result['edges'] = None
            result['verts'] = list(self.elements)
        return result


if __name__ == '__main__':
    # Make use of our classes.
    verts = {i: Vertex(i) for i in range(5)}

    # Edges
    edges = {7: Edge(7, [verts[1], verts[4]], front=True),
             11: Edge(11, [verts[3], verts[2]], front=True),
             13: Edge(13, [verts[4], verts[3]], front=True),
             14: Edge(14, [verts[2], verts[1]], front=True),
             }

    c1 = Face(1, [edges[7], edges[13], edges[11], edges[14]])
    c2 = Face(2, [verts[1], verts[4], verts[3], verts[2]])

    print c1.contents()
    print c2.contents()
