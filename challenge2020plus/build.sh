#!/usr/bin/bash

# This script can be called from conda-execute:
#
# $ ~avd/live/conda-execute/bin/conda-execute -vf build.sh

# conda execute
# env:
#  - pip

# use pip as you cannot force conda-execute to use conda-forge
pip install datapane pygithub plotly

# Create the conda env for dev purposes:
#   $ conda create -c conda-forge -n iris_stats datapane pygithub plotly

# capture command line options (if any) so we can pass it thru
CLA_OPTIONS=$*

python challenge2020plus.py ${CLA_OPTIONS}