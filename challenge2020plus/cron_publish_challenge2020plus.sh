#!/usr/bin/env bash

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)
cd ${SCRIPT_DIR}

# Documentation deployment parent directory
TARGET_REPORT_DIR="/home/h06/avd/public_html/challenge2020plus"

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Create log file
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOG_FILE=${LOG_DIR}/challenge2020plus_report.$(date +%Y-%m-%d-%H%M%S).log
cat <<EOF >${LOG_FILE}
Creating  Challenge 2020+ report.
DATE:  $(date)
OUTPUT ...
EOF

# The Python script to execute.
SCRIPT=${SCRIPT_DIR}/build.sh

# The entry-point to conda-execute.
EXECUTE=~avd/live/conda-execute/bin/conda-execute

# Execute the script within a conda-execute controlled environment.
export CONDA_ENVS_PATH=${LOCALTEMP}/conda/publish_chalenge2020plus/envs

# conda execute the report script
${EXECUTE} -vf ${SCRIPT} >>${LOG_FILE} 2>&1

RETURN_CODE=${?}

# Check exit status of build script.
if [ ${RETURN_CODE} -ne 0 ]
then
    cat ${LOG_FILE}
    echo "challenge2020plus.py failed.  Build Aborted!" >>${LOG_FILE}
    echo "** Contact Tremain Knight **" >>${LOG_FILE}
    exit ${RETURN_CODE}
fi

# Copy the report to the publish directory
echo "Copying report to publish directory: ${TARGET_REPORT_DIR}" >>${LOG_FILE}
cp -r challenge2020plus.html ${TARGET_REPORT_DIR}
cp -r data ${TARGET_REPORT_DIR}

RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]
then
    cat ${LOG_FILE}
    echo "challenge2020plus.py failed.  Build Aborted!" >>${LOG_FILE}
    echo "** Contact Tremain Knight **" >>${LOG_FILE}
    exit ${RETURN_CODE}
fi

echo "Done." >>${LOG_FILE}