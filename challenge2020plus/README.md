# Challenge 2020+

This project is a simple python program that will query various sources
including the GitHub API for data on releases, issues, pull requests
and commits.

The published report html can be found here:
[https://www-avd/~avd/challenge2020plus/challenge2020plus.html](https://www-avd/~avd/challenge2020plus/challenge2020plus.html)


### GitHub API

In order to use the GitHub API a token needs to be present.  This python
program will look to this token in `~/.api_keys/github-token.txt`


## Sources of data

   * In `challenge2020plus.py`
      * **REPOS**.  List of GitHub repos to query
   * In `get_git_data.py`
      * **RELEASES_SSS_CSV_URL**.  SSS release csv list as a url
      * **RELEASES_OTHER**.  For any non queryable release information.


## Developer Quickstart

* Create a conda environment, for the spec see `build.sh` (conda execute)
* Run `python challenge2020plus.py`.  Create the report

Rerunning the report will overwrite any local cached data (such as GitHub)
that is created in the `data` subdirectory.

There are variables in the scripts that can change the behvaiour when
developing:

* **QUERY_GITHUB**
  When being run as a cron this should always be set to
  **True**.  However is you are testing the report and not the data
  querying this can be set to **False**.  This will also prevent any
  rate limiting on the API token use.

* **DEBUG**
  Set to **True** or **False**.  This will show more information
  to stdout, including previews of pandas dataframes.  Should be set to
  **False** when not developing.

* **INFO**
  Set to **True** or **False**.  This will show high level
  information on what the program is doing.  Useful to enable so there
  is a hint at what may be thr issue if a run fails. Should be set to
  **True** always unless you want a silent run.



### Source & Scripts

* `challenge2020pluse.py`.  Main program.
* `get_git_data.py`.  Library used for fetching all data
