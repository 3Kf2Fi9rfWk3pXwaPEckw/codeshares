import numpy as np
import numpy.random as nr
from scipy.misc import imread
from glob import glob
import os
from sklearn import svm, linear_model
import multiprocessing as mp
import sha
from sklearn.externals import joblib
from PIL import Image


DATA_BASEPATH='/data/users/dkirkham/sklearn_dash/iris_visual_tests_data'
GOOD_DATA = '/data/users/dkirkham/sklearn_dash/iris_visual_tests_data/known_good'
CACHE_DIR='/data/users/dkirkham/classifier_cache'
TRAINING_PROPORTION = .8
RES_X, RES_Y = 800, 600
RNG_SEED=423141

def get_filenames(*args):
    args = args +  ('*.png',)
    glob_str = os.path.join(*args)
    files = glob(glob_str)
    return [ fname for fname in files if Image.open(fname).size == (RES_X, RES_Y) ]

def load_dataset(basepath):
    known_good_files = get_filenames(GOOD_DATA)
    good_files = get_filenames(basepath, 'good', '*')
    bad_files = get_filenames(basepath, 'bad', '*')
    all_files = good_files + bad_files
    nr.seed(RNG_SEED)
    shuffled_indices = range(len(all_files))
    nr.shuffle(shuffled_indices)
    print 'Good count:', len(good_files)
    print 'Bad count: ', len(bad_files)
    pgood = float(len(good_files)) / len(all_files) * 100
    print 'Good%/Bad%: {:.4}/{:.4}'.format(pgood, 100 - pgood)
    target = np.append(np.ones(len(good_files)), np.zeros(len(bad_files)))
    target = target[shuffled_indices]
    data_size = RES_X * RES_Y
    data = np.empty((len(all_files), data_size), dtype=np.double)

    known_good_data = np.empty((len(known_good_files), RES_Y, RES_X), dtype=np.double) 
    known_good_index_dict = {}
    for i in range(len(known_good_files)):
        img = imread(known_good_files[i], flatten=True)
        img = img.astype(np.double)
        known_good_data[i].flat = img
        known_good_index_dict[known_good_files[i]] = i

    for i, k in enumerate(shuffled_indices):
        fpath = all_files[k]
        fname = os.path.basename(fpath)
        good_fpath = os.path.join(GOOD_DATA , fname)
        img = imread(fpath, flatten=True)
        img = img.astype(np.double)
        j = known_good_index_dict[good_fpath]
        data[i].flat = img - known_good_data[j]
    return data, target

def get_key(data, target, training_size, clf_type, args, kwargs):
    key = sha.sha(data)
    key.update(target)
    key.update(str(training_size))
    key.update(str(clf_type))
    key.update(np.array(args))
    kwargs_list = kwargs.items()
    kwargs_list.sort()
    key.update(np.array(kwargs_list))
    return key.hexdigest()

def train_new_classifier(data, target, training_size, clf_type, *args, **kwargs):
    clf = clf_type(*args, **kwargs)
    print 'Training estimator...'
    return clf.fit(data[:training_size], target[:training_size])

def get_classifier(data, target, training_size, clf_type, *args, **kwargs):
    # Try loading the classifier from a cache.
    key = get_key(data, target, training_size, clf_type, args, kwargs)
    path = os.path.join(CACHE_DIR, key)
    try:
        clf, _ = joblib.load(path)
        print 'Loaded from cache'
    except:
        print 'Not found in cache'
        clf = train_new_classifier(data, target, training_size, clf_type, *args, **kwargs)
        joblib.dump((clf, 0), path)
    return clf

def run_tests(clf, testing_data, testing_target):

    print 'Predicting data...'
    pred = clf.predict(testing_data)
    print 'Test targets =', testing_target
    print 'Predictions =', pred
    accuracy = sum(pred == testing_target) /  float(len(testing_target)) * 100
    print 'Correct predictions:', sum(pred == testing_target)
    print 'Overall test accuracy: {:.4}%'.format(accuracy)

    good_tgt_pred = pred[testing_target == 1]
    if len(good_tgt_pred) > 0:
        false_negative_percentage = sum(good_tgt_pred == 0) /  float(len(good_tgt_pred)) * 100
        print 'Predictions for good test data =', good_tgt_pred
        print 'Total:', len(good_tgt_pred)
        print 'False negatives:', sum(good_tgt_pred == 0)
        print 'False negative percentage: {:.4}%'.format(false_negative_percentage)

    bad_tgt_pred = pred[testing_target == 0]
    if len(bad_tgt_pred) > 0:
        false_positive_percentage = sum(bad_tgt_pred) /  float(len(bad_tgt_pred)) * 100
        print 'Predictions for bad test data =', bad_tgt_pred
        print 'Total:', len(bad_tgt_pred)
        print 'False positives:', sum(bad_tgt_pred)
        print 'False positive percentage: {:.4}%'.format(false_positive_percentage)

def main():
    print 'Loading dataset...'
    data, target = load_dataset(DATA_BASEPATH)
    training_size = int(round(len(data) * TRAINING_PROPORTION))
    print 'Dataset size: ', len(data)
    print 'Training size:', training_size
    print 'Testing size: ', len(data) - training_size
    clf = get_classifier(data, target, training_size, linear_model.SGDClassifier)

    print 'Training data accuracy: {:.4}%'.format(clf.score(data[:training_size], target[:training_size]) * 100)

    testing_data = data[training_size:]
    testing_target = target[training_size:]
    run_tests(clf, testing_data, testing_target)

if __name__ == '__main__':
    main()
