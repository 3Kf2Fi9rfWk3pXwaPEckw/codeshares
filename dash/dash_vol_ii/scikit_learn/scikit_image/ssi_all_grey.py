'''
example using skimage SSI on all the AVD test plot data (greyscale version)

this code trawls through a directory of images, tries to find

a corresponding known good image then computes the MSE and SSI

and writes to stdout in a csv format

'''

import numpy as np
import matplotlib.pyplot as plt

from skimage import data, img_as_float
from skimage.measure import compare_ssim as ssim

import glob

def mse(x, y):
    return np.linalg.norm(x - y)

goodImagePath = '../good/known_good/'

joiner = '.'


# Process bad images

dataPath = '../good/'

# get a list of directories 
badDirList = glob.glob(dataPath + '*')


for d in badDirList:

   if d != '../good/known_good':

     print "Directory", d
     fileList = glob.glob(dataPath + d + '/*')
   
     for f in fileList:
        # try and find the known good image for this image
        searchStr = '*' + joiner.join(f.split('.')[-4:-1]) + '*'
        #print dataPath + d + '/' + searchStr
        kg = glob.glob(goodImagePath + searchStr)
        #print f
        #print kg[0]
        # ignore any where there are multiple or no matches
        if len(kg) == 1:
          known_good_img = data.imread(kg[0], as_grey=True)
          test_img = data.imread(f, as_grey=True)
          mse_test = mse(known_good_img, test_img)
          ssim_test = ssim(known_good_img, test_img,
                  data_range=test_img.max() - test_img.min())
          print searchStr + "," + str(mse_test) + ',' + str(ssim_test) 

