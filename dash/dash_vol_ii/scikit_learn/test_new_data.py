import os
from glob import glob
import re
from sklearn.externals import joblib
import datetime

from test_images import load_dataset, run_tests

NEW_DATA_BASEPATH='/data/users/dkirkham/sklearn_dash/new'
CACHE_DIR = '/data/users/dkirkham/classifier_cache'
SHA_REGEX = re.compile('^[0-9a-fA-F]{40}$')

def load_clf(filename):
    clf, _ = joblib.load(filename)
    return clf

def main():
    paths = [os.path.join(CACHE_DIR, sha) for sha in os.listdir(CACHE_DIR) if SHA_REGEX.match(sha)]
    stats = [(os.stat(path), path) for path in paths]
    stats.sort(reverse=True, key=lambda (stat, _): stat.st_ctime)
    while True:
        for (i, (stat, _)) in enumerate(stats):
            print '{:<3}  {:}'.format(i, datetime.datetime.fromtimestamp(stat.st_ctime).strftime('%H:%M %d/%m/%y'))

        x = raw_input('Enter a number: ')
        try:
            x = int(x)
            if not 0 <= x < len(stats):
                raise ValueError()
            break
        except ValueError:
            print 'Enter an integer in the range 0-{}'.format(len(stats)-1)
            pass
    
    clf = load_clf(stats[x][1])
    data, target = load_dataset(NEW_DATA_BASEPATH)
    run_tests(clf, data, target)

if __name__ == '__main__':
    main()
