#!/bin/bash -l
#SBATCH --mem=500
#SBATCH --ntasks=4
#SBATCH --error=/scratch/itwl/dask/job.%N.%j.err
#SBATCH --output=/scratch/itwl/dask/job.%N.%j.out
#SBATCH --time=10

BASE=~/miniconda2/bin
PORT=8786

echo "master: $(hostname) $(hostname -i) $$ $PPID"

${BASE}/dask-scheduler --port ${PORT} &

srun -n 2 ${SCRATCH}/dask/worker.sh "$(hostname -i):${PORT}"
