"""Wrap datashader functionality to work in matplotlib."""

import datashader as ds
import datashader.transfer_functions as tf


# ###
# Thanks, @mdboom!
# (See https://gist.github.com/mdboom/048aa35df685fe694330764894f0e40a)
# ###
class LiveImageDisplay(object):
    def __init__(self, df, agg_col_1, agg_col_2, h=500, w=500,
                 agg_reduction=None, agg_reduction_col=None, interp_how=None):
        self.df = df
        self.agg_col_1 = agg_col_1
        self.agg_col_2 = agg_col_2
        self.height = h
        self.width = w
        self.agg_reduction = agg_reduction
        self.agg_reduction_col = agg_reduction_col
        self.interp_how = interp_how

    def __call__(self, xstart, xend, ystart, yend):
        # Set up a datashader_utils canvas.
        canvas = ds.Canvas(plot_width=self.width,
                           plot_height=self.height,
                           x_range=(xstart, xend),
                           y_range=(ystart, yend))

        # Projection & Aggregation Step.
        if self.agg_reduction is not None:
            reductor = getattr(ds, self.agg_reduction)
            agg = canvas.points(self.df, self.agg_col_1, self.agg_col_2,
                                reductor(self.agg_reduction_col))
        else:
            agg = canvas.points(self.df, self.agg_col_1, self.agg_col_2)

        # (Optional) Transformation Step.
        result = agg
        if self.interp_how is not None:
            img = tf.interpolate(agg, how=self.interp_how)
            result = img
        return result

    def ax_update(self, ax):
        ax.set_autoscale_on(False)  # Otherwise, infinite loop

        # Get the number of points from the number of pixels in the window
        dims = ax.axesPatch.get_window_extent().bounds
        self.width = int(dims[2] + 0.5)
        self.height = int(dims[2] + 0.5)

        # Get the range for the new area
        xstart, ystart, xdelta, ydelta = ax.viewLim.bounds
        xend = xstart + xdelta
        yend = ystart + ydelta

        # Update the image object with our new data and extent
        im = ax.images[-1]
        im.set_data(self.__call__(xstart, xend, ystart, yend))
        im.set_extent((xstart, xend, ystart, yend))
        ax.figure.canvas.draw_idle()
