"""Load data from metdb as pandas dataframes and plot with datashader."""

import glob
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import mpl_wrapper


def metdb_to_df(files, channel):
    dframes = []
    for pkl_path in files:
        inflated = pd.read_pickle(pkl_path)
        df = pd.DataFrame({'obs': inflated['BRGTS_TMPR'][:, channel-1],
                           'lat': inflated['LTTD'],
                           'lon': inflated['LNGD']})
        dframes.append(df)
    return pd.concat(dframes)


def binning(df):
    """Bin a dataframe."""
    bins = np.arange(-90, 90, 30)


def plot(dataframe):
    # Variables.
    xmin = -180.0
    ymin = -90.0
    xmax = 180.0
    ymax = 90.0

    img = mpl_wrapper.LiveImageDisplay(dataframe, 'lon', 'lat', 500, 500,
                                       agg_reduction='mean',
                                       agg_reduction_col='obs')
    z = img(xmin, xmax, ymin, ymax)
    # res = z.to_masked_array()
    # out = np.ma.masked_equal(res, 0)

    vir_cm = plt.get_cmap(name='jet', lut=10)
    vir_cm.set_under('w')
    vir_cm.set_bad('w')

    fig1, ax2 = plt.subplots(1, 1)
    m = ax2.imshow(z, origin='lower', extent=(xmin, xmax, ymin, ymax),
                   cmap=vir_cm, vmin=0.1)
    plt.colorbar(m)

    # Connect for changing the view limits
    ax2.callbacks.connect('xlim_changed', img.ax_update)
    ax2.callbacks.connect('ylim_changed', img.ax_update)

    plt.show()


def main():
    fp = '/data/users/dkillick/data/sa_avd'
    channel = 9
    pkls = glob.glob(os.path.join(fp, '*.pkl'))
    print pkls
    big_df = metdb_to_df(pkls[:5], channel)
    print big_df
    plot(big_df)


if __name__ == '__main__':
    main()
