# Sources for the ng-vat "regrid demo"

For delivery to "NG-VAT 0.1" milestone, which demonstrates LFRic-to-UM regridding
See : https://jira:6391/browse/AVD-1414

## Usage Notes

### To build+deploy the test environment
  * Run *only* as avd user :  Use `su - avd`
  * Run the (build) script `[here]/scripts/build_ngvat-regrid-demo_environment.sh`
     * it makes or cleans out the temporary directory `$TMPDIR/build_ngvat_regrid_env`
     * it puts the built environment directly into `/project/avd/ng-vat/envrionments/ngvat_regrid_demo_env`
  * Run the (test) script `[here]/scripts/test_ngvat-regrid-demo_python-iris-ugrid.sh`
     * this checks that : correct Iris and Iris-ugrid are installed ;
       user demo scripts are present ; iris-ugrid tests are passing.

### To run the regrid demo
  * cd to a writeable directory
  * Run the script `[working]/ngvat_regrid_demo/ngvat_regrid_demo_env/bin/spice_run_regrid.sh`
    * this script demonstrates the use of `iris_ugrid.regrid`.
      Orography data is extracted from an LFRic output (XIOS) file, then regridded
      onto a global lon-lat grid (derived from a typical UM output dataset).
    * it will take several minutes to run.
    * temporary and result files are created in the current directory.
  * the core code is in `[working]/ngvat_regrid_demo/ngvat_regrid_demo_env/bin/regrid_xios_to_um.py`
    * feel free to explore and modify your copy of this script to get a better
      idea of how things work.
    * this _can_ be run directly, on VDI, but it may a very long time, or crash, due to running out of resources
    * the script also contains code to target a much smaller target latlon.
      This operation is easily enabled and _is_ VDI-safe

