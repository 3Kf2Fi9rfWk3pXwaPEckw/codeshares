"""
Plotting example using pyvista
------------------------------

This script demonstrates the plotting of a "surface temperature" field from
a sample UGRID file, using PyVista.  It is hoped that a user can adapt the
example here to plot their own data.

This code was used to demonstrate the pyvista plotting in the NG-VAT "Shape" demonstration.

Setup:
------
To run this code, first install the pyvista environment using:
    $ ./pyvista-installer.sh

Activate the python environment by:
    $ export PATH="/path/to/environment/pyvista/miniconda3/envs/mamba/bin:${PATH}"
    $ . activate pyvista

Code details:
-------------
This code contains 2 functions and 1 class.

  * mesh_from_nc (function)
      This function loads in a netcdf file and uses the variables to create a PyVista mesh.
      It may need to be modified depending on the input file you are using as there may be
      some minor differences (e.g how missing data is represented, names of variables). 
      N.B. the function includes two workarounds which are particular to the type of files 
      it was used on: faces with missing data are skipped to make the code more efficient, 
      and faces that straddle the antimeridian are skipped as they cause problems with plotting.

  * get_coastlines (function)
      This function loads in shapefiles that define coastlines, and create PyVista Line objects.

  * PolydataTransformFilter (class)
      This class creates a VTK transformer that takes any polydata object (e.g. the mesh, or
      the coastlines) and transforms it from a lat/lon projection to the given projection.


"""

import cartopy.io.shapereader as shp
from cartopy.io.shapereader import Record
import netCDF4 as nc
import numpy as np
import pyvista as pv
from shapely.geometry.multilinestring import MultiLineString
import vtk


__version__ = "0.1.0.dev0"


class PolydataTransformFilter:
    """
    A VTK transformer that can project pyvista objects from lat/lon projection
    to a given projection e.g. Molleweide.

    """
    def __init__(self, proj_name='moll'):
        """
        Args:
        * proj_name:
            the target projection. This should be a proj4 string
            e.g. moll, sinu
        
        """
        # Set up source and target projection
        sourceProjection = vtk.vtkGeoProjection()
        destinationProjection = vtk.vtkGeoProjection()
        destinationProjection.SetName(proj_name)

        # Set up transform between source and target.
        transformProjection = vtk.vtkGeoTransform()
        transformProjection.SetSourceProjection(sourceProjection)
        transformProjection.SetDestinationProjection(destinationProjection)

        # Set up transform filter
        transform_filter = vtk.vtkTransformPolyDataFilter()
        transform_filter.SetTransform(transformProjection)
        
        self.transform_filter = transform_filter
    
    def transform(self, mesh):
        self.transform_filter.SetInputData(mesh)
        self.transform_filter.Update()
        output = self.transform_filter.GetOutput()
        
        # Wrap output of transform as a Pyvista object.
        return pv.wrap(output)


def mesh_from_nc(fname):
    """
    Loads a netcdf file and creates a pyvista PolyData mesh.

    Note: this function makes some assumptions about the input file

    """
    # Load the dataset in the netcdf file
    ds = nc.Dataset(fname)

    # Set the names of the variables in the nc file that we want.
    data_name = 'surface_temperature'
    face_node = 'dynamics_face_nodes' # the face node connectivity (e.g. face 1 is made of nodes <4, 5, 6, 7>)
    x = 'dynamics_node_x' # longitudes of the nodes
    y = 'dynamics_node_y' # latitudes of the nodes

    # Get the the data and face node connectivity variables.
    data_var = ds.variables[data_name]
    face_node_var = ds.variables[face_node]

    # Get the array from the data and face node connectivity variable.
    data = data_var[:].data
    node_face = face_node_var[:].data

    # Select a single time slice of the data if data contains multiple times.
    if len(data.shape) == 2:
        data = data[0]

    # WORKAROUND:
    # Handle missing data by skipping faces with missing data.
    # Get the Fill Value
    if hasattr(data_var, "_FillValue"):
        fill_value = data_var._FillValue
        # Get the indices of the faces with missing data.
        faces_with_real_data = data != fill_value
        # Subset the data and node face connectivity array to only faces with data
        node_face = node_face[faces_with_real_data]
        data = data[faces_with_real_data]

    # Get start index
    start_index = face_node_var.start_index

    # Get the longitudes and latitudes of the nodes.
    node_x = ds.variables[x][:].data
    node_y = ds.variables[y][:].data

    # Convert longitudes from (0, 360) to (-180, 180)
    node_x[node_x > 180] -=360

    # Face_node connectivity may be 1 based counting if start index is 1
    node_face -= start_index

    # WORKAROUND:
    # Skip faces that straddle the -180/+180 lines as these cause problems when
    # plotting.
    # For each face, get the longitude of its nodes
    lon_of_face_nodes = node_x[node_face] # e.g. [[-45, -43, -43, -45],...]
    # Find the difference between the node with the min longitude value and the
    # node with the max longitude value
    diff = lon_of_face_nodes.max(axis=1) - lon_of_face_nodes.min(axis=1)
    diffs_less_than_180 = diff < 180
    # Select only faces whose nodes span a longitude difference of <180
    node_face = node_face[diffs_less_than_180]
    data = data[diffs_less_than_180]

    # Create PolyData:
    # To create a PolyData object, we need vertices of the shape
    # (x_location, y_locations, z_locations). 
    z_zeros = np.zeros(len(node_x))
    vertices = np.vstack([node_x, node_y, z_zeros]).T

    # Pyvista requires each face entry to start with the number of nodes it
    # contains. For example,
    #   a square face with nodes 0,1,2,3 would be [4, 0, 1, 2, 3]
    #   a triangular face with nodes 0,1,2 would be [3, 0, 1, 2]
    # So, we need to prepend our face node connectivity array with 4's.
    face_4 = 4 * np.ones(len(node_face)).reshape((len(node_face), 1))
    faces = np.hstack([face_4[:], node_face[:]])
    faces = np.ravel(faces)

    # Create the PolyData object, ensuring the faces index are integers
    mesh = pv.PolyData(vertices, faces.astype(np.int))
    
    # Add data to cell faces
    mesh.cell_arrays[data_name] = data[:]

    return mesh


def get_coastlines(resolution="110m"):
    """
    Return PyVista line objects using shapefiles that define the coastlines.

    Modified version of 
    https://github.com/bjlittle/poc-ngvat/blob/master/poc-3/utils.py

    """
    # Load in the shapefiles
    fname = shp.natural_earth(resolution=resolution,
                              category="physical",
                              name="coastline")
    reader = shp.Reader(fname)

    dtype = np.float32
    blocks = pv.MultiBlock()
    geoms = []

    def to_pyvista_blocks(records):
        for record in records:
            if isinstance(record, Record):
                geometry = record.geometry
            else:
                geometry = record

            if isinstance(geometry, MultiLineString):
                geoms.extend(list(geometry.geoms))
            else:
                xy = np.array(geometry.coords[:], dtype=dtype)
                x = xy[:,0].reshape(-1, 1)
                y = xy[:,1].reshape(-1, 1)
                z = np.zeros_like(x)

                xyz = np.hstack((x, y, z))
                poly = pv.lines_from_points(xyz, close=False)
                blocks.append(poly)

    to_pyvista_blocks(reader.records())
    to_pyvista_blocks(geoms)

    return blocks


if __name__=="__main__":
    
    # Load in the data and mesh from the netcdf file.
    mesh = mesh_from_nc('/net/spice/project/avd/ng-vat/data/poc-03/real/qrclim.sst.ugrid.nc')
    
    # Create the PyVista line objects that define the coastlines.
    coastlines = get_coastlines('50m')
    
    # Set up Mollweide transformer.
    moll_transformer = PolydataTransformFilter('moll')
    
    # Reproject the mesh from platecarree to a mollweide projection.
    moll_mesh = moll_transformer.transform(mesh)
    
    # Reproject the coastlines
    moll_coastlines = [moll_transformer.transform(block) for block in coastlines]
   
    # Create a plotter object and add the mesh to it.
    p = pv.Plotter()
    p.add_mesh(moll_mesh,
               show_edges=True, edge_color='white', line_width=0.5,
               scalar_bar_args=dict(color='black', label_font_size=16,
                                    n_labels=6, title_font_size=18))
    
    # Add coastlines:
    for block in moll_coastlines:
        p.add_mesh(block, color="black")
    
    p.background_color = 'white'
    p.camera_position = [(93959.85410932079, 328636.4707611543, 50654659.22149858),
                     (93959.85410932079, 328636.4707611543, 0.0),
                     (0.0, 1.0, 0.0)]
    p.show()
