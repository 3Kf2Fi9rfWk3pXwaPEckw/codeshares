#!/usr/bin/env bash

################################################################################
#
# This is a script for downloading and installing miniconda, and creating a
# conda/mamba environment containing the relevant dependencies for the pyvista
# package.
#
# In addition, the pyvista github repo is downloaded to make available its
# numerous example scripts, and the pyvista-examples github repo is also
# downloaded for access to its jupyter notebook pyvista examples.
#
# This installer also creates a README.txt for the user and an environment.yml
# file of the top level conda package that will be installed into the
# pyvista environment.
#
# This is a development script and this installer should not be used other
# than for development purposes.
#
# AVD Team, December 2020.
#
# (c) Crown copyright Met Office. All rights reserved.
#
################################################################################

PROG=$(basename ${0} .sh)
PROG_VERSION=0.1.2

DEFAULT_NUMPY_VERSION="1.19.5"
DEFAULT_PYTHON_VERSION="3.9"
DEFAULT_PYVISTA_VERSION="0.27.3"


div() {
  local COLS

  # show a divider the exact width of the terminal
  COLS=$(tput cols)
  echo -e "${BRIGHT}"
  printf "=%.0s" $(eval "echo {1..$((${COLS}))}")
  echo -e "${RESET}"
}


log() {
  local MSG="${1}"

  # show a divider and log message
  div
  echo -e "${GREEN}${BRIGHT}${PROG}${RESET}: ${MSG}\n"
}


msg() {
  local MSG="${1}"

  # show a message
  echo -e "${GREEN}${BRIGHT}${PROG}${RESET}: ${MSG}"
}


usage() {
  cat <<EOF

Usage: ${PROG} [-c] [-d] [-n <numpy-version>] [-p <python-version>] [-u] [-v <pyvista-version>] [--version] [-h]

Create a Python environment containing PyVista and its dependencies.
Also download Jupyter notebook and Python script examples for PyVista.

The environment will be installed into the directory "${BASE_DIR}".

The PyVista environment will be created with mamba (default) or conda.

-h, --help     Show this help message and exit.

-c, --conda    Install the environment using conda.
               Default is "mamba" [faster].

-d, --dirty    Don't remove the previous environment before re-installation.
               Default is to delete environment prior to installation.

-n, --numpy    Version of NumPy to be installed.
               Default is "${DEFAULT_NUMPY_VERSION}".

-p, --python   Version of Python to be installed.
               Default is "${DEFAULT_PYTHON_VERSION}".

-u, --update   Update conda to the latest available version.

-v, --vista    Version of PyVista to be installed.
               Default is "${DEFAULT_PYVISTA_VERSION}".

    --version  Show the installer version.

EOF

  exit 0
}


#
# configure defaults
#
ROOT_DIR=$(pwd)
BASE_DIR=$(pwd)/pyvista
CONDA_BASE_DIR=${BASE_DIR}/miniconda3
CONDA_BIN_DIR=${CONDA_BASE_DIR}/bin
DOWNLOADS_DIR=${BASE_DIR}/downloads
ENV_YML=${BASE_DIR}/environment.yml
README=${BASE_DIR}/README.txt
SRC_DIR=${BASE_DIR}/src

DOWNLOAD_MINICONDA_URL=https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
NUMPY_VERSION=${DEFAULT_NUMPY_VERSION}
PYTHON_VERSION=${DEFAULT_PYTHON_VERSION}
PYVISTA_VERSION=${DEFAULT_PYVISTA_VERSION}
PYVISTA_ENV_NAME="pyvista"
PYVISTA_ENV_CHANNEL="conda-forge"
PYVISTA_EXAMPLES_NOTEBOOK_DIR=${BASE_DIR}/pyvista-examples-notebook
PYVISTA_EXAMPLES_SCRIPT_DIR=${BASE_DIR}/pyvista-examples-script
DOWNLOAD_PYVISTA_URL=https://github.com/pyvista/pyvista.git
DOWNLOAD_PYVISTA_EXAMPLES_URL=https://github.com/pyvista/pyvista-examples.git

OPTION_CLEAN=true
OPTION_MAMBA=true
OPTION_UPDATE=

BRIGHT="\x1b[1m"
GREEN="\x1b[32m"
RED="\x1b[31m"
NORMAL="\x1b[39m"
RESET="\x1b[39m\x1b[22m"


#
# parse the command line arguments
#
OPTIONS=$(getopt -l "help,conda,dirty,python:,update,vista:,version" -o "hcdp:uv:" -a -- "${@}")

eval set -- "${OPTIONS}"

while true
do
  case ${1} in
  -h|--help)
    usage
    ;;
  -c|--conda)
    OPTION_MAMBA=
    ;;
  -d|--dirty)
    OPTION_CLEAN=
    ;;
  -n|--numpy)
    NUMPY_VERSION="${1}"
    ;;
  -p|--python)
    shift
    PYTHON_VERSION="${1}"
    ;;
  -u|--update)
    OPTION_UPDATE=true
    ;;
  -v|--vista)
    shift
    PYVISTA_VERSION="${1}"
    ;;
  --version)
    echo -e "\n${GREEN}${BRIGHT}${PROG}${RESET} version ${PROG_VERSION}\n"
    exit 0
    ;;
  --)
    shift
    break
    ;;
  esac

shift
done

#
# check opengl version
#
GLXINFO=$(glxinfo 2>/dev/null | grep -i "opengl version string:")
OPENGL_VERSION=$(echo "${GLXINFO}" | cut -d" " -f4)

log "Detecting OpenGL version..."

OPENGL_MAJOR=$(echo ${OPENGL_VERSION} | cut -d"." -f1)
OPENGL_MINOR=$(echo ${OPENGL_VERSION} | cut -d"." -f2)
OPENGL_ABORT=

[ "${OPENGL_MAJOR}" -lt "3" ] && OPENGL_ABORT=True
[ "${OPENGL_MAJOR}" -eq "3" ] && [ "${OPENGL_MINOR}" -lt "2" ] && OPENGL_ABORT=True

if [ -n "${OPENGL_ABORT}" ]
then
  msg "${BRIGHT}Found OpenGL ${RED}v${OPENGL_VERSION}${RESET}"
  msg "${BRIGHT}Require at least OpenGL ${GREEN}v3.2${NORMAL}"
  msg "${BRIGHT}${RED}Aborting! 💥👎😢${RESET}\n"
  exit 1
else
  msg "${BRIGHT}Found OpenGL ${GREEN}v${OPENGL_VERSION}${RESET} 👍"
fi

#
# check for existing conda on PATH
#
which conda >/dev/null 2>&1
RC="${?}"

log "Detecting conda..."

if [ "${RC}" -eq 0 ]
then
  msg "${BRIGHT}Found conda installation ${RED}$(which conda)${RESET}"
  msg "${BRIGHT}Please remove this from your ${RED}PATH${NORMAL} environment variable${RESET}"
  msg  "${BRIGHT}${RED}Aborting! 💥👎😢\n"
  exit 1
else
  msg "${BRIGHT}No conda installation found 👍"
fi

#
# trap for this point onwards
#
trap 'echo -e "\n${GREEN}${BRIGHT}${PROG}${RESET}: ${RED}${BRIGHT}Aborted!${RESET} 💥👎😢\n"; exit 1' ERR
set -e

#
# installer session summary
#
if [ -n "${OPTION_MAMBA}" ]
then
  MODE_MAMBA="${BRIGHT}${GREEN}${OPTION_MAMBA}${RESET}"
else
  MODE_MAMBA="${BRIGHT}${RED}false${RESET}"
fi

if [ -z "${OPTION_CLEAN}" ]
then
  MODE_DIRTY="${BRIGHT}${GREEN}true${RESET}"
else
  MODE_DIRTY="${BRIGHT}${RED}false${RESET}"
fi

if [ -n "${OPTION_UPDATE}" ]
then
  MODE_UPDATE="${BRIGHT}${GREEN}${OPTION_UPDATE}${RESET}"
else
  MODE_UPDATE="${BRIGHT}${RED}false${RESET}"
fi

div
msg "[dirty=${MODE_DIRTY}] [mamba=${MODE_MAMBA}] [numpy=${BRIGHT}${GREEN}${NUMPY_VERSION}${RESET}] [python=${BRIGHT}${GREEN}${PYTHON_VERSION}${RESET}] [pyvista=${BRIGHT}${GREEN}${PYVISTA_VERSION}${RESET}] [update=${MODE_UPDATE}]"

#
# check whether to remove previous installation
#
[ -n "${OPTION_CLEAN}" ] && [ -d "${BASE_DIR}" ] && rm -rf ${BASE_DIR}

#
# create the base directory et al
#
mkdir -p ${BASE_DIR}
mkdir -p ${DOWNLOADS_DIR}
cd ${BASE_DIR}

#
# download and install miniconda
#
log "Downloading miniconda..."
time wget ${DOWNLOAD_MINICONDA_URL} -O miniconda.sh

log "Installing miniconda..."
time bash miniconda.sh -b -p ${CONDA_BASE_DIR}
export PATH=${CONDA_BIN_DIR}:${PATH}
mv miniconda.sh ${DOWNLOADS_DIR}

log "Configuring conda..."
conda config --set always_yes yes --set changeps1 no
conda config --set show_channel_urls True
conda config --add channels conda-forge

if [ -n "${OPTION_UPDATE}" ]
then
  log "Updating conda..."
  time conda update --quiet --name base conda
fi

#
# create the environment.yml file
#
log 'Creating "environment.yml" file...'

if [ -n "${OPTION_MAMBA}" ]
then
  cat <<EOF >${ENV_YML}
cartopy
colorcet
jupyter
matplotlib
netcdf4
numpy=${NUMPY_VERSION}
python=${PYTHON_VERSION}
pyvista=${PYVISTA_VERSION}
pyvistaqt
scipy
EOF
else
  cat <<EOF >${ENV_YML}
name: ${PYVISTA_ENV_NAME}

channels:
  - ${PYVISTA_ENV_CHANNEL}

dependencies:
  - cartopy
  - colorcet
  - jupyter
  - matplotlib
  - netcdf4
  - numpy=${NUMPY_VERSION}
  - python=${PYTHON_VERSION}
  - pyvista=${PYVISTA_VERSION}
  - pyvistaqt
  - scipy

EOF
fi

cat ${ENV_YML}
msg "See ${BRIGHT}${ENV_YML}${RESET}"

#
# create the conda pyvista environment
#
if [ -n "${OPTION_MAMBA}" ]
then
  log "Creating mamba conda environment..."
  time conda create -n mamba -c conda-forge mamba
  . activate mamba

  log "Creating pyvista conda environment..."
  time mamba create -n ${PYVISTA_ENV_NAME} -c ${PYVISTA_ENV_CHANNEL} --file=${ENV_YML}
  export CONDA_BIN_DIR=${CONDA_PREFIX}/bin
else
  log "Creating pyvista conda environment${INSTALLER}..."
  time conda env create --file=${ENV_YML}
fi

#
# download pyvista examples
#

log "Downloading pyvista src examples..."
mkdir ${SRC_DIR}
cd ${SRC_DIR}
time git clone ${DOWNLOAD_PYVISTA_URL}
cd pyvista
git checkout ${PYVISTA_VERSION}
ln -s ${SRC_DIR}/pyvista/examples ${PYVISTA_EXAMPLES_SCRIPT_DIR}

log "Downloading notebook examples..."
cd ${BASE_DIR}
time git clone ${DOWNLOAD_PYVISTA_EXAMPLES_URL} pyvista-examples-notebook

#
# create the README.txt file
#

log "Creating README.txt..."
cat <<EOF >${README}
********************************************************************************
*
* :file: ${README}
*
* This :file: was auto-generated by "${PROG}.sh" @ $(date)
*
********************************************************************************

--------------------
Activate environment
--------------------

Activate the conda pyvista environment as follows:

  $ export PATH="${CONDA_BIN_DIR}:\${PATH}"
  $ . activate pyvista


---------
YAML file
---------

The YAML file that created the conda pyvista environment is:

  ${ENV_YML}

using the conda command:

  $ conda env create --file=${ENV_YML}

Add additional packages to the conda pyvista environment as follows:

  $ conda install -n pyvista <package-name>


----------------
PyVista examples
----------------

PyVista is complemented with a rich suite a examples both in Jupyter notebook
and Python script format.

The Jupyter notebook pyvista examples are available in the directory:

  - "${PYVISTA_EXAMPLES_NOTEBOOK_DIR}"

Note that, jupyter notebook has been installed as part of this conda pyvista
environment.

The Python script pyvista examples are available in the directory:

  - "${PYVISTA_EXAMPLES_SCRIPT_DIR}"

For further information see https://docs.pyvista.org/examples/index.html


----------------------
Deactivate environment
----------------------

Deactivate the conda pyvista environment as follows:

  $ conda deactivate


----------------
Rinse and repeat
----------------

To create a separate but similar conda installation to this one, simply
copy the "pyvista-installer.sh" and re-execute it a separate directory.


---------------
Further reading
---------------
  - https://docs.pyvista.org/index.html
  - https://docs.pyvista.org/examples/index.html
  - https://docs.pyvista.org/plotting/plotting.html?highlight=keyboard

Enjoy!

EOF

msg "See ${BRIGHT}${README}${RESET}"

#
# salutation
#

log "Installation complete"

msg "Enjoy pyvista ${BRIGHT}${GREEN}v${PYVISTA_VERSION}${RESET} 👍"

#
# execute example
#

log "Running pyvista example..."

EXAMPLE_SCRIPT=${PYVISTA_EXAMPLES_SCRIPT_DIR}/00-load/read-dolfin.py

msg "${GREEN}${BRIGHT}export PATH=${CONDA_BIN_DIR}:\${PATH}${RESET}"
msg "${GREEN}${BRIGHT}. activate pyvista${RESET}"
msg "${GREEN}${BRIGHT}python ${EXAMPLE_SCRIPT}${RESET}\n"

. activate pyvista
python ${EXAMPLE_SCRIPT}

#
# see the readme.txt
#

log "Installation completed!"

msg "For instructions on how to use this PyVista environment, please see ${GREEN}${BRIGHT}${README}${RESET}\n"
