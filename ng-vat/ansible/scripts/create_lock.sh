#!/usr/bin/env bash

PROG=$(basename ${0} .sh)
PROG_VERSION=0.1.0


div() {
  local COLS

  # show a divider the exact width of the terminal
  COLS=$(tput cols)
  echo -e "${BRIGHT}"
  printf "=%.0s" $(eval "echo {1..$((${COLS}))}")
  echo -e "${RESET}"
}


log() {
  local MSG="${1}"

  # show a divider and log message
  if [ -z "${OPTION_QUIET}" ]
  then
    div
    echo -e "${GREEN}${BRIGHT}${PROG}${RESET}: ${BRIGHT}${MSG}${RESET}\n"
  fi
}


msg() {
  local MSG="${1}"

  # show a message
  if [ -z "${OPTION_QUIET}" ]
  then
    echo -e "${GREEN}${BRIGHT}${PROG}${RESET}: ${BRIGHT}${MSG}${RESET}"
  fi
}


usage() {
  cat <<EOF

Usage: ${PROG} [-f <file>] [-n <name>] [-h] [-q] [-v] 

Create a conda-lock file of the requirements file.

-h, --help       Show this help message and exit.
-f, --file       The requirements filename (input).
-n, --name       The lock name (output).
-q, --quiet      Silence output.
-v, --version    Show verison.

EOF

  exit 0
}


oops() {
  local EMSG="${1}"

  msg "${RED}ERROR${NORMAL} - ${EMSG}\n"

  exit 1
}


################################################################################

#
# directory of this script
#
DIR_SCRIPT=$(cd "$(dirname ${0})"; pwd;)


#
# configure the defaults.
#
DIR_RESOURCES=$(dirname ${DIR_SCRIPT})/resources
REQUIREMENTS_FNAME=
LOCK_NAME=

BRIGHT="\x1b[1m"
GREEN="\x1b[32m"
RED="\x1b[31m"
NORMAL="\x1b[39m"
RESET="\x1b[39m\x1b[22m"

OPTION_QUIET=

#
# cli: parse the command line arguments
#
OPTIONS=$(getopt -l "help,file:,name:,quiet,version" -o "hf:n:qv" -a -- "${@}")

eval set -- "${OPTIONS}"

while true
do
  case ${1} in
  -h|--help)
    usage
    ;;
  -f|--file)
    shift
    export REQUIREMENTS_FNAME="${1}"
    ;;
  -n|--name)
    shift
    export LOCK_NAME="${1}"
    ;;
  -q|--quiet)
    export OPTION_QUIET="--quiet"
    ;;
  -v|--version)
    msg "version ${PROG_VERSION}"
    exit 0
    ;;
  --)
    shift
    break
    ;;
  esac

shift
done


#
# cli: ensure no extra command line arguments
#
[ "${#}" -ne 0 ] && usage


#
# cli: check requirements filename
#
if [ -z "${REQUIREMENTS_FNAME}" ]
then
  oops "Missing requirements filename"
fi

if [ ! -f "${REQUIREMENTS_FNAME}" ]
then
  oops "Cannot open requirements file \"${REQUIREMENTS_FILE}\""
fi


#
# cli: check lock name
#
if [ -z "${LOCK_NAME}" ]
then
  oops "Missing lock name"
fi


#
# trap from this point onwards
#
trap 'echo -e "\n${GREEN}${BRIGHT}${PROG}${RESET}: ${RED}${BRIGHT}Aborted!${RESET} 💥👎😢\n"; exit 1' ERR
set -e


#
# create resource directory
#
if [ -d "${DIR_RESOURCES}" ]
then
  msg "removing pre-existing resources directory ${GREEN}${DIR_RESOURCES}"
  rm -rf ${DIR_RESOURCES}
fi

if [ ! -d "${DIR_RESOURCES}" ]
then
  msg "creating resources directory ${GREEN}${DIR_RESOURCES}"
  mkdir -p ${DIR_RESOURCES}
fi


#
# create the conda lock
#
log "creating conda lock..."

LOCK_FNAME="${DIR_RESOURCES}/${LOCK_NAME}-linux-64.lock"
[ -f "${LOCK_FNAME}" ] && rm -f ${LOCK_FNAME}
FS_DEV=/dev/fd/1

if [ -n "${OPTION_QUIET}" ]
then
  FS_DEV=/dev/null
fi

BASE_LOCK_FNAME=$(basename ${LOCK_FNAME})
TEMPLATE="${LOCK_NAME}-{platform}.lock"
conda-lock -f ${REQUIREMENTS_FNAME} -p linux-64 --filename-template ${TEMPLATE} >${FS_DEV} 2>&1
[ ! -f "${BASE_LOCK_FNAME}" ] && oops "Failed to create conda lock file ${RED}${BASE_LOCK_FNAME}"
mv ${BASE_LOCK_FNAME} ${LOCK_FNAME}
msg "created resources conda lock file ${GREEN}${LOCK_FNAME}"
