#!/usr/bin/env python3

import click
from collections.abc import Mapping
import pathlib
import yaml


@click.command()
@click.argument(
    "requirement",
    type=click.File("w"),
)
@click.option(
    "-f",
    "--file",
    "fnames",
    type=click.Path(),
    multiple=True,
    help="path to conda environment YAML file(s)",
)
def cli(requirement, fnames):
    # pip package requirements
    packages = []

    if fnames:
        fnames = [pathlib.Path(fname) for fname in fnames]

    for fname in fnames:
        # load the conda environment yaml requirements
        with open(fname, "r") as fi:
            spec = yaml.load(fi, Loader=yaml.FullLoader)
        # retrieve the package dependencies
        dependencies = spec.get("dependencies", None)
        if dependencies:
            # check for pip package dependencies
            for mapping in filter(lambda item: isinstance(item, Mapping), dependencies):
                if "pip" in mapping:
                    packages.extend(mapping["pip"])

    packages = "\n".join(packages)

    # write the package dependencies to the requirements file
    requirement.write(packages)


if __name__ == "__main__":
    cli()
