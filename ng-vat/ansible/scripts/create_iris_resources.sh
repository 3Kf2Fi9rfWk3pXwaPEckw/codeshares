#!/usr/bin/env bash

PROG=$(basename ${0} .sh)
PROG_VERSION=0.1.0


manifest() {
  local CHECK="${1}"
  local FILES
  local FILE
  local MANIFEST_FILE=${DIR_RESOURCES}/manifest
  local RESULT=0
  local MANIFEST_COUNT

  if [ -n "${CHECK}" ]
  then
    sha256sum ${OPTION_QUIET} --check ${MANIFEST_FILE}
    RESULT=${?}

    if [ ${RESULT} -eq 0 ]
    then
      MANIFEST_COUNT=$(wc -l ${MANIFEST_FILE} | cut -d' ' -f1)
      if [ -z "${EXTRA_YAML_FILE}" ] && [ ${MANIFEST_COUNT} -eq 5 ]
      then
        RESULT=1
      elif [ -n "${EXTRA_YAML_FILE}" ] && [ ${MANIFEST_COUNT} -eq 4 ]
      then
        RESULT=1
      else
        if [ -n "${EXTRA_YAML_FILE}" ]
        then
          cd ${DIR_CWD}
          ACTUAL=$(sha256sum ${EXTRA_YAML_FILE} | cut -d' ' -f1)
          EXPECTED=$(tail -n1 ${MANIFEST_FILE} | cut -d' ' -f1)
          [ "${ACTUAL}" != "${EXPECTED}" ] && RESULT=1
        fi
      fi
    fi
  else
    >${MANIFEST_FILE}
    declare -a FILES=(${TGT_REQ_FILE} ${TGT_LOCK_FILE} ${TGT_TARBALL_FILE} ${TGT_PIP_FILE})
    for FILE in "${FILES[@]}"
    do
      echo "$(sha256sum ${FILE})" >> ${MANIFEST_FILE}
    done

    if [ -n "${EXTRA_YAML_FILE}" ]
    then
      FILE=${DIR_RESOURCES}/$(basename ${EXTRA_YAML_FILE})
      echo "$(sha256sum ${FILE})" >> ${MANIFEST_FILE}
    fi

    msg "created resources manifest file ${GREEN}${MANIFEST}"
  fi

  # ensure a numeric result
  return $((RESULT))
}


div() {
  local COLS

  # show a divider the exact width of the terminal
  COLS=$(tput cols)
  echo -e "${BRIGHT}"
  printf "=%.0s" $(eval "echo {1..$((${COLS}))}")
  echo -e "${RESET}"
}


log() {
  local MSG="${1}"

  # show a divider and log message
  if [ -z "${OPTION_QUIET}" ]
  then
    div
    echo -e "${GREEN}${BRIGHT}${PROG}${RESET}: ${BRIGHT}${MSG}${RESET}\n"
  fi
}


msg() {
  local MSG="${1}"

  # show a message
  if [ -z "${OPTION_QUIET}" ]
  then
    echo -e "${GREEN}${BRIGHT}${PROG}${RESET}: ${BRIGHT}${MSG}${RESET}"
  fi
}


usage() {
  cat <<EOF

Usage: ${PROG} [-h] [-c] [-e <file-name> ] [-p <python-version>] [-q] [-v] <artifact>

Create a conda-lock file of the iris requirements file associated with the
SciTools/iris GitHub repository artifact.

-h, --help       Show this help message and exit.
-c, --clean      Ignore any existing resources cache.
-e, --extra      Additional YAML requirements file.
-p, --python     Set the iris requirements file Python version.
                 Defaults to "${DEFAULT_CI_PYTHON}".
-q, --quiet      Silence output.
-v, --version    Show verison.
artifact         SciTools/iris GitHub repository artifact, which may
                 be a branch, a tag or a commit SHA (mandatory).

EOF

  exit 0
}


oops() {
  local EMSG="${1}"

  msg "${RED}ERROR${NORMAL} - ${EMSG}\n"

  exit 1
}


################################################################################

#
# directory of this script
#
DIR_SCRIPT=$(cd "$(dirname ${0})"; pwd;)
DIR_CWD=${PWD}


#
# configure the defaults.
#
DEFAULT_CI_PYTHON="38"
CI_PYTHON=${DEFAULT_CI_PYTHON}
DIR_RESOURCES=$(dirname ${DIR_SCRIPT})/resources
IRIS_GITHUB="https://github.com/scitools/iris.git"

ARTIFACT_FILE=${DIR_RESOURCES}/artifact
COMMIT_FILE=${DIR_RESOURCES}/commit
TGT_REQ_FILE=${DIR_RESOURCES}/iris.yml
TGT_LOCK_FILE=${DIR_RESOURCES}/iris-linux-64.lock
TARBALL_FILE=iris.tar.bz2
TGT_TARBALL_FILE=${DIR_RESOURCES}/${TARBALL_FILE}
TGT_PIP_FILE=${DIR_RESOURCES}/requirements.txt

BRIGHT="\x1b[1m"
GREEN="\x1b[32m"
RED="\x1b[31m"
NORMAL="\x1b[39m"
RESET="\x1b[39m\x1b[22m"

OPTION_CLEAN=
OPTION_QUIET=

#
# parse the command line arguments
#
OPTIONS=$(getopt -l "help,clean,extra:,python:,quiet,version" -o "hce:p:qv" -a -- "${@}")

eval set -- "${OPTIONS}"

while true
do
  case ${1} in
  -h|--help)
    usage
    ;;
  -c|--clean)
    export OPTION_CLEAN="true"
    ;;
  -e|--extra)
    shift
    export EXTRA_YAML_FILE="${1}"
    ;;
  -p|--python)
    shift
    export CI_PYTHON="${1}"
    ;;
  -q|--quiet)
    export OPTION_QUIET="--quiet"
    ;;
  -v|--version)
    msg "version ${PROG_VERSION}"
    exit 0
    ;;
  --)
    shift
    break
    ;;
  esac

shift
done


#
# parse the mandatory command line argument
#
[ "${#}" -ne 1 ] && usage
ARTIFACT="${1}"


#
# determine whether to skip, as artifact is cached
#
if [[ -f "${ARTIFACT_FILE}" && -z "${OPTION_CLEAN}" ]]
then
  log "checking artifact resource cache..."

  read -r CACHE<${ARTIFACT_FILE}

  if [ "${CACHE}" == "${ARTIFACT}" ]
  then
    manifest "check"
    if [ ${?} -eq 0 ]
    then
      msg "artifact ${GREEN}${ARTIFACT}${NORMAL} cache hit, ${GREEN}skipping!"
      exit 0
    fi
  fi

  msg "artifact ${GREEN}${ARTIFACT}${NORMAL} cache miss!"
fi


#
# trap from this point onwards
#
trap 'echo -e "\n${GREEN}${BRIGHT}${PROG}${RESET}: ${RED}${BRIGHT}Aborted!${RESET} 💥👎😢\n"; exit 1' ERR
set -e


#
# create a temporary workspace
#
DIR_TMP=$(mktemp -d -t "${USER}-$(date +%Y-%m-%d-%H%M%S)-XXXXXXXX")
log "created temporary workspace ${GREEN}${DIR_TMP}"
DIR_IRIS=${DIR_TMP}/iris
mkdir -p ${DIR_IRIS}


#
# clone and checkout iris
#
log "cloning and checking out SciTools/iris artifact..."

msg "targeting ${GREEN}${ARTIFACT}"
git clone ${OPTION_QUIET} ${IRIS_GITHUB} ${DIR_IRIS}
cd ${DIR_IRIS}
git fetch ${OPTION_QUIET} origin
git checkout ${OPTION_QUIET} ${ARTIFACT}
COMMIT=$(git rev-parse HEAD)
msg "HEAD commit is ${GREEN}${COMMIT}" 


#
# extract iris requirements file
#
log "extracting iris requirements file..."

SRC_REQ_FILE=requirements/ci/py${CI_PYTHON}.yml
msg "targeting ${GREEN}${SRC_REQ_FILE}"

if [ -d "${DIR_RESOURCES}" ]
then
  msg "removing pre-existing resources directory ${GREEN}${DIR_RESOURCES}"
  rm -rf ${DIR_RESOURCES}
fi

if [ ! -d "${DIR_RESOURCES}" ]
then
  msg "creating resources directory ${GREEN}${DIR_RESOURCES}"
  mkdir -p ${DIR_RESOURCES}
fi

[ -f "${TGT_REQ_FILE}" ] && rm -f ${TGT_REQ_FILE}
cp -f ${SRC_REQ_FILE} ${TGT_REQ_FILE} 
msg "created resources requirements file ${GREEN}${TGT_REQ_FILE}"

if [ -n "${EXTRA_YAML_FILE}" ]
then
  cd ${DIR_CWD}

  if [ ! -f "${EXTRA_YAML_FILE}" ]
  then
    oops "Missing extra YAML requirements file, ${GREEN}${EXTRA_YAML_FILE}"
  else
    cp ${EXTRA_YAML_FILE} ${DIR_RESOURCES}
    msg "copied extra YAML requirements file ${GREEN}${EXTRA_YAML_FILE}"
  fi
fi


#
# cache the latest commit sha
#
log "creating resources cache..."

echo "${ARTIFACT}" > ${ARTIFACT_FILE}
msg "created resources artifact file ${GREEN}${ARTIFACT_FILE}"
echo "${COMMIT}" > ${COMMIT_FILE}
msg "created resources commit file ${GREEN}${COMMIT_FILE}"


#
# create the conda lock
#
log "creating iris conda lock..."

cd ${DIR_RESOURCES}
[ -f "${TGT_LOCK_FILE}" ] && rm -f ${TGT_LOCK_FILE}
FS_DEV=/dev/fd/1
[ -n "${OPTION_QUIET}" ] && FS_DEV=/dev/null

TMP_PIP_FILE=${DIR_RESOURCES}/iris-pip-requirements.yml
cat <<EOF >${TMP_PIP_FILE}
channels:
  - conda-forge
dependencies:
  - pip
EOF

ARGS="--file ${TGT_REQ_FILE} --file ${TMP_PIP_FILE}"
[ -n "${EXTRA_YAML_FILE}" ] && ARGS="${ARGS} --file ${EXTRA_YAML_FILE}"

time conda-lock ${ARGS} --mamba --platform linux-64 --filename-template "iris-{platform}.lock" >${FS_DEV} 2>&1
[ -f "${TMP_PIP_FILE}" ] && rm -f ${TMP_PIP_FILE}
[ ! -f "${TGT_LOCK_FILE}" ] && oops "Failed to create conda lock file ${RED}${TGT_LOCK_FILE}"
msg "created resources conda lock file ${GREEN}${TGT_LOCK_FILE}"


#
# create pip requirements
#
log "creating pip requirements.txt..."

cd ${DIR_RESOURCES}
[ -f "${TGT_PIP_FILE}" ] && rm -f ${TGT_PIP_FILE}
ARGS="--file ${TGT_REQ_FILE}"
[ -n "${EXTRA_YAML_FILE}" ] && ARGS="${ARGS} --file ${EXTRA_YAML_FILE}"
${DIR_SCRIPT}/make_pip_requirements.py ${TGT_PIP_FILE} ${ARGS}


#
# create iris tarball
#
log "creating iris tarball..."

cd ${DIR_TMP}
rm -rf ${DIR_IRIS}/.git
SRC_TARBALL_FILE=${DIR_TMP}/${TARBALL_FILE}
tar cjf ${TARBALL_FILE} $(basename ${DIR_IRIS})
msg "created tarball ${GREEN}${SRC_TARBALL_FILE}"
[ -f "${TGT_TARBALL_FILE}" ] && rm -rf ${TGT_TARBALL_FILE}
cp ${TARBALL_FILE} ${TGT_TARBALL_FILE}
msg "created resources tarball file ${GREEN}${TGT_TARBALL_FILE}"


#
# create the resources manifest
#
log "creating resources manifest..."

manifest


#
# remove temporary workspace
#
log "tidying temporary workspace..."
rm -rf ${DIR_TMP}
