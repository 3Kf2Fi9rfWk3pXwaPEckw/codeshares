---

- name: Prepare iris resources
  hosts: localhost
  vars_prompt:
    - name: artifact
      prompt: Specify the SciTools/iris artifact to install
      default: mesh-data-model
      private: no

    - name: env_name
      prompt: Specify the name of the iris conda environment to create
      default: mesh-data-model
      private: no

    - name: file_name
      prompt: Specify additional requirements YAML file
      private: no

  tasks:
    - name: Setting fact
      set_fact: yaml='{{ file_name if file_name else "" }}'

    - name: Create iris resources
      shell: scripts/create_iris_resources.sh --quiet '{{ artifact }}'
      when: yaml is falsy(convert_bool=True)

    - name: Create iris resources - extra
      shell: scripts/create_iris_resources.sh --quiet --extra='{{ yaml }}' '{{ artifact }}'
      when: yaml is truthy(convert_bool=True)

    - command: echo '{{ env_name }}' 
      register: env_name
      changed_when: env_name.stdout == ''


- name: Transfer iris resources
  hosts: '{{ target }}'
  tasks:
    - name: Check install base directory
      stat:
        path: '{{ base_dir }}'
      register: check_install_dir

    - name: Fail on missing installation
      fail:
        msg: 'mambaforge has not been installed, missing {{ base_dir }}'
      when: check_install_dir.stat.exists == false

    - name: Synchronize resources
      ansible.posix.synchronize:
        src: resources
        dest: '{{ base_dir }}' 


- name: Create conda environment and install iris
  hosts: '{{ target }}'
  tasks:
    - name: Check iris conda environment
      stat:
        path: "{{ envs_dir }}/{{ hostvars['localhost']['env_name'].stdout }}"
      register: check_conda_env
      when: hostvars['localhost']['env_name'] is defined

    - name: Create iris conda environment
      shell: "{{ bin_dir }}/mamba create --name {{ hostvars['localhost']['env_name'].stdout }} --quiet --file {{ resources_dir }}/iris-linux-64.lock"
      when:
        - hostvars['localhost']['env_name'] is defined
        - check_conda_env.stat.exists == false

    - name: Install pip requirements
      shell: "source {{ bin_dir }}/activate {{ hostvars['localhost']['env_name'].stdout }} && pip install --requirement {{ resources_dir }}/requirements.txt"
      when:
        - hostvars['localhost']['env_name'] is defined
        - check_conda_env.stat.exists == false

    - name: Unarchive iris tarball
      unarchive:
        src: '{{ resources_dir }}/iris.tar.bz2'
        dest: '{{ resources_dir }}'
      when: check_conda_env.stat.exists == false

    - name: Install iris
      shell: "source {{ bin_dir }}/activate {{ hostvars['localhost']['env_name'].stdout }} && pip install --no-deps {{ resources_dir }}/iris"
      when:
        - hostvars['localhost']['env_name'] is defined
        - check_conda_env.stat.exists == false
    
    - name: Tidy unarchived iris
      file:
        path: '{{ resources_dir }}/iris'
        state: absent
      when: check_conda_env.stat.exists == false

...
