## Ansible for NG-VAT

### What is this Codeshare?

This codeshare provides an [Ansible](https://docs.ansible.com/) orchestration framework for managing the
deployment and configuration of NG-VAT conda environvments across the estate. This **excludes** the HPC.

This capability is organised into [Ansible playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html).


### What is Ansible?

Ansible is a radically simple IT automation engine that automates cloud provisioning, configuration management,
application deployment, intra-service orchestration, and many other IT needs.

Being designed for multi-tier deployments, Ansible models your IT infrastructure by describing how all your systems
inter-relate, rather than just managing one system at a time.

It uses no agents and no additional custom security infrastructure, so it's easy to deploy - and most importantly, it uses
a very simple language (YAML, in the form of Ansible Playbooks) that allow you to describe your
automation jobs in a way that approaches plain English.

#### Ansible Concepts

The following concepts are common to all uses of Ansible. You need to understand them to use Ansible for any kind of automation.

##### Control Node

Any machine with Ansible installed. You can run Ansible commands and playbooks by invoking the `ansible` or `ansible-playbook`
from any control node. You may have multiple control nodes, all of which must have Python installed. Note that, a `Windows`
host cannot be a control node.

##### Managed Nodes

The network devices (and/or servers) you manage with Ansible. Managed nodes are also sometimes called "hosts". Ansible is not
installed on managed nodes.

##### Inventory

A list of managed nodes, rather like a traditional hosts file.

##### Collections

Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins.
You can install and use collections through [Ansible Galaxy](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html).

##### Modules

The units of code Ansible executes. Each module has a specific use. You invoke a single module with a task, or invoke several
different modules in a playbook. Ansible works by connecting to your manages nodes and pushing out module scripts to them for
execution.

##### Plugins

[Plugins](https://docs.ansible.com/ansible/latest/plugins/plugins.html#plugins-lookup) augment Ansible's core functionality.
Whilst modules execute on the target system in separate processes, plugins execute on the control node within the `/usr/bin/ansible`
process. Plugins offer options and extensions for the core features of Ansible. Ansible ships with a number of batteries included
plugins, but you can easily [write your own](https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#developing-plugins).
Plugins must be written in Python.

##### Tasks

The units of action in Ansible. You can execute a single task once with an ad-hoc command.

##### Playbooks

An ordered list of tasks, saved so you can run those tasks in that order repeatedly. Playbooks can inclue variables as well
as tasks, and are written in YAML. To learn more about playbooks, see [Intro to playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#about-playbooks).


### What's Included in this Codeshare?

Here's the lay of the land...

```
ansible/
|-- README.md                       <-- This file ;-)
|-- ansible.cfg                     <-- Ansible configuration file
|-- conda-install.yml               <-- Ansible playbook
|-- conda-uninstall.yml             <-- Ansible playbook
|-- env-create-iris.yml             <-- Ansible playbook
|-- env-create.yml                  <-- Ansible playbook
|-- env-remove.yml                  <-- Ansible playbook
|-- environment.yml                 <-- Conda environment to run this codeshare
|-- group_vars
|   `-- all                         <-- Ansible configuration file for global facts
|-- host_vars                       
|   |-- els056.yml                  <-- Ansible configuration file for els056
|   `-- localhost.yml               <-- Ansible configuration file for the localhost (127.0.0.1) i.e., the Ansible control node
|-- inventory.yml                   <-- Ansible configuration file for known hosts
|-- scripts
|   |-- create_iris_resources.sh    <-- Bash script to harvest iris GitHub artifact requirements
|   |-- create_lock.sh              <-- Bash script to create a conda-lock file
|   `-- make_pip_requirements.py    <-- Python script to extract pip package requirements from conda environment YAML files
`-- v0p2.yml                        <-- Additional release package dependencies

```


### Codeshare Package Requirements

Before running this codeshare, first install its package dependencies as follows:

```shell
> wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
> chmod u+x Miniconda3-latest-Linux-x86_64.sh
> ./Miniconda3-latest-Linux-x86_64.sh -b -p miniconda3
> export PATH="${PWD}/miniconda3/bin:${PATH}"
> conda env create --file environment.yml
> . activate ansible-dev
```

You will also require to install the [ansible.posix.synchronize](https://docs.ansible.com/ansible/latest/collections/ansible/posix/synchronize_module.html) plugin:

```shell
> ansible-galaxy collection install ansible.posix
```


### Ansible Playbooks in this Codeshare

#### conda-install.yml

This playbook will create a conda installation from [mambaforge](https://github.com/conda-forge/miniforge#mambaforge)
under the target host directory `/tmp/persistent/<user>/ng-vat/miniforge3`.

The miniforge installation from mambaforge has a smaller footprint than the traditional [miniconda](https://docs.conda.io/en/latest/miniconda.html)
installation, and also comes included with [mamba](https://github.com/mamba-org/mamba)
batteries included.

#### conda-uninstall.yml

This playbook uninstalls the complete conda installation under the target host
`/tmp/persistent/<user>/ng-vat` directory.

#### env-create-iris.yml

This playbook installs a GitHub iris source version, and its associated package dependencies,
into a target host conda environment.

It will interactively prompt you for:

- The GitHub [SciTools/iris](https://github.com/SciTools/iris) **<artifact>** to install.
  This may be a branch name, a tag name or a commit SHA
- The **<env-name>** of the target host conda environment to create

The playbook will use [conda-lock](https://github.com/conda-incubator/conda-lock) to generate
fully reporducible conda lock files, targeting the `linux-64` platform. A lock file is generated
from the requirements YAML file associated with the chosen iris **<artifact>**. Using a lock
file ensures that all target hosts install **identical packages** in their conda environments.

The playbook also generates the iris source tarball from the specified **<artifact>**, which iris is
installed from using [pip](https://pip.pypa.io/en/stable/).

All resources generated by the local host control node are transfered to the target host
`/tmp/persistent/<user>/ng-vat/resources` directory using the Ansible [synchronize](https://docs.ansible.com/ansible/latest/collections/ansible/posix/synchronize_module.html)
plugin. This directory contains the following resources:

```
resources
|-- artifact              <-- The origin artifact, provided by the user
|-- commit                <-- The GitHub commit SHA of the artifact
|-- iris-linux-64.lock    <-- The conda-lock lock file
|-- iris.tar.bz2          <-- The iris artifiact source tarball
|-- iris.yml              <-- The iris requirements YAML file
`-- manifest              <-- A sha256sum resource file manifest
```

The `iris-linux-64.lock` file is used by [mamba](https://github.com/mamba-org/mamba) to create the
**<env-name>** conda environment, and [pip](https://pip.pypa.io/en/stable/) is then used to install
iris from the unarchived `iris.tar.bz2` tarball.


#### env-create.yml

This playbook installs conda package dependencies into a target host conda environment.

It will interactively prompt you for:

- The YAML filename specifying the packages of the conda environment
- The name of the target host conda environment


#### env-remove.yml

This playbook will interactively prompt you for the **<name>** of the conda environment to uninstall from the target host.


### How to run an Ansible Playbook

Executing an ansible playbook couldn't be easier. Use the [ansible-playbook](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html)
command to execute tasks on the target hosts e.g.,

```shell
> ansible-playbook conda-install.yml --extra-vars "target=avd"
```

Ensure to run `ansible-playbook` in the directory containing the `ansible.cfg` file.
Also see [Connection methods and details](https://docs.ansible.com/ansible/latest/user_guide/connection_details.html)


### How to run an Ansible Ad-Hoc Command

To executing an [Ansible ad-hoc command](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)
use the [Ansible](https://docs.ansible.com/ansible/latest/cli/ansible.html) command to execute tasks on
the target host e.g.,

```shell
> ansible -m ping avd
```

Ensure to run `ansible` in the directory containing the `ansible.cfg` file.
Also see [Connection methods and details](https://docs.ansible.com/ansible/latest/user_guide/connection_details.html)


### Host Inventory

Ansible works with multiple known managed nodes or target hosts, which are declared in the `inventory.yml` file.

See [How to build your inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) for further details.

The playbooks of this codeshare require to use the `--extra-vars` keyword argument to `ansible-playbook`
to specify the **<target>** hosts e.g.,

```shell
> ansible-playbook iris-create.yml --extra-vars "target=opengl"
```

See [Patterns: targeting hosts and groups](https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html) for further
details on how to specify host patters.


### Outstanding Issues

* Get Ansible working when the existing user has performed an `su - avd`
  * use `--ask-become-password`

### Future Work

* Investigate the use of roles
* Investigate converting the `create_iris_resources.sh` and `create_lock.sh` bash scripts into Ansible plugins

 
### Further Reading
The following references helped with the initial development of this framework. There are however plentiful other
resources available for [Ansible](https://docs.ansible.com/):

- https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html#vars-and-facts
    - ansible localhost -m ansible.builtin.setup

- https://docs.ansible.com/ansible/latest/collections/ansible/posix/synchronize_module.html

- https://ansibledaily.com/idempotent-shell-command-in-ansible/

- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/stat_module.html

- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#ansible-collections-ansible-builtin-file-module

- https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html#conditions-based-on-registered-variables

- https://stackoverflow.com/questions/33896847/how-do-i-set-register-a-variable-to-persist-between-plays-in-ansible

- https://stackoverflow.com/questions/33222641/override-hosts-variable-of-ansible-playbook-from-the-command-line

