"""
Find the unique STASH and LBPROC combinations.

"""

import argparse
import collections
import glob
import itertools
import os
import os.path
import re
import sys
import time
import warnings

from iris.coords import DimCoord
import iris
import numpy as np


# The `catalogue` maps from a Phenomenon to a "distribution".

Phenomenon = collections.namedtuple('Phenomenon', ['stash', 'lbproc'])

# A "distribution" is a map from stream name to a forecast-period -> z-values
# map.
#
# distribution = {
#     'd': {
#         3: set([1000, 950, 900, ... 10]),
#         6: set([1000, 950, 900, ... 10]),
#         ...
#         168: set([1000, 900, ... 400])},
#     'j': { ... }
# }


def add_lbproc(cube, field, filename):
    cube.attributes['LBPROC'] = int(field.lbproc)


def catalogue_path(path, catalogue, pattern):
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        cubes = iris.load_raw(path, callback=add_lbproc)
    stream = pattern.match(os.path.basename(path)).group(1)
    for cube in cubes:
        try:
            fp = cube.coord('forecast_period')
            assert fp.shape == (1,)
            fp = fp.points[0]
        except iris.exceptions.CoordinateNotFoundError:
            # This has to be a number so that the seequence abbrevation
            # code works, so just set it to something that won't occur.
            fp = -1000

        z = cube.coords(axis='Z')
        z = [c for c in z if c.units.is_vertical() and isinstance(c, DimCoord)]
        if z:
            if len(z) != 1:
                import pdb; pdb.set_trace()
            assert len(z) == 1
            z, = z
            assert z.shape == (1,)
            z = z.points[0]
        else:
            z = ()
        phenomenon = Phenomenon(cube.attributes['STASH'],
                                cube.attributes['LBPROC'])
        distribution = catalogue.setdefault(phenomenon, {})
        # Use a set to ignore repeated values from multiple vertical
        # levels.
        forecast_periods = distribution.setdefault(stream, {})
        levels = forecast_periods.setdefault(fp, set())
        if z:
            levels.add(z)


def catalogue_paths(paths, pattern):
    catalogue = {}
    print 'Reading:',
    n = len(paths)
    t0 = time.time()
    for i, path in enumerate(paths):
        msg = 'Reading {} of {}: {}'.format(i + 1, n, os.path.basename(path))
        ti = time.time()
        if i > 0:
            td = ti - t0
            msg += ' ETA: {:.0f} s'.format(td * (n - i) / i)
        print '\r' + msg + ' ' * 10,
        sys.stdout.flush()
        catalogue_path(path, catalogue, pattern)
    print
    return catalogue


def find_paths(dir_path, pattern):
    names = sorted(os.listdir(dir_path))
    paths = [os.path.join(dir_path, name) for name in names
             if pattern.match(name)]
    return paths


def abbreviate_sequence(forecast_periods):
    forecast_periods = list(reversed(forecast_periods))
    d1 = np.around(np.diff(forecast_periods), 4)
    groups = [(d, sum(1 for v in g)) for d, g, in itertools.groupby(d1)]
    result = []
    # The number needed in a pattern before we abbreviate.
    N = 5
    chopped = False
    float_fmt = '{:.3g}'
    abbrev_fmt = '{}, {}, ..., {}'.format(float_fmt, float_fmt, float_fmt)
    for d, n in groups:
        if chopped: n -= 1
        if n >= N - 1:
            result.append(abbrev_fmt.format(forecast_periods[n],
                                            forecast_periods[n - 1],
                                            forecast_periods[0]))
            del forecast_periods[:n + 1]
            chopped = True
        else:
            result.extend(float_fmt.format(fp) for fp in forecast_periods[:n])
            del forecast_periods[:n]
            chopped = False
    result.extend(float_fmt.format(fp) for fp in forecast_periods)
    result.reverse()
    return result


def has_consistent_levels(time_to_levels_map):
    common_levels = None
    for levels in time_to_levels_map.itervalues():
        if common_levels is None:
            common_levels = levels
        elif levels != common_levels:
            return False
    return True

def level_union(time_to_levels_map):
    all_levels = set()
    for levels in time_to_levels_map.itervalues():
        all_levels.update(levels)
    return all_levels

def distribution_as_mediawiki(distribution):
    lines = []
    for stream in sorted(distribution.keys()):
        time_to_levels_map = distribution[stream]
        forecast_periods = abbreviate_sequence(sorted(time_to_levels_map))
        if has_consistent_levels(time_to_levels_map):
            levels = time_to_levels_map.values()[0]
            if levels:
                levels = ', '.join('{:.5g}'.format(v) for v in sorted(levels))
            else:
                levels = ''
        else:
            levels = level_union(time_to_levels_map)
            levels = 'crazy levels ' + ', '.join('{:.5g}'.format(v) for v in sorted(levels))
        if levels:
            fmt = '{} -> {} @ <br/> <div style="padding-left: 1em">{}</div>'
            line = fmt.format(stream, '; '.join(forecast_periods), levels)
        else:
            line = '{} -> {}'.format(stream, '; '.join(forecast_periods))
        lines.append(line)
    return ' <br/> '.join(lines)


def show_as_mediawiki(wiki_path, catalogue):
    with open(wiki_path, 'w') as f:
        f.write('{| class="wikitable sortable"\n')
        f.write('|-\n')
        f.write('! STASH code !! LBPROC !! Distribution\n')
        fmt = ('| <div id="{stash}-{lbproc}"></div> '
               '[http://reference.metoffice.gov.uk/um/stash/{stash} {stash}] '
               '|| {lbproc} || {distribution}\n')
        for phenomenon, distribution in sorted(catalogue.iteritems()):
            f.write('|-\n')
            distribution = distribution_as_mediawiki(distribution)
            f.write(fmt.format(stash=phenomenon.stash,
                               lbproc=phenomenon.lbproc,
                               distribution=distribution))
        f.write('|}\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Catalogue the contents of model output FieldsFiles.')
    parser.add_argument('--dir_path', default='.',
                        help='path to directory of model outputs, default: .')
    parser.add_argument('--pattern', default='.*_p([a-z])\d{3}',
                        help='Python regexp pattern to match model output '
                             'files, with a group to match the stream, '
                             'default: .*_p([a-z])\d{3}')
    parser.add_argument('--wiki_path', default='wiki.txt',
                        help='path for wiki output, default: wiki.txt')
    args = parser.parse_args()
    pattern = re.compile(args.pattern + '$')
    paths = find_paths(args.dir_path, pattern)
    catalogue = catalogue_paths(paths, pattern)
    show_as_mediawiki(args.wiki_path, catalogue)
