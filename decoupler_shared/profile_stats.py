from __future__ import print_function
import re
import argparse
import sys
import os
import glob
import pstats
from collections import namedtuple
import warnings
from itertools import chain

import numpy as np

import log_parse

# The pstat.Stats object returned by show_prof.aggregate() is not very well
# doumented, so I will provide some info here.
#
# The object contains information about all the calls made in the program,
# where they happened, the time they took and the number which were made. The
# time taken is aggregated over all calls so it's not possible to determine the
# time taken by a particular method call.
#
# The stats attribute is a dictionary with keys of the form
# (filepath, line, function) and values which are tuples of the form
# (primitive calls, recursive calls, tottime, cumtime, dictionary) where
# tottime and  cumtime are the same as specified here:
# https://docs.python.org/2.7/library/profile.html#instant-user-s-manual.
# The dictionaries have keys of the same form, and values representing
# statistics of calls to the function defined by the key. These values have the
# form:
# (primitive calls, recursive calls, tottime, cumtime)
#
# After a call to calc_callees, the all_callees attribute contains a dictionary
# similar to the above, except that the values are dictionaries representing
# all calls *from* the key function

# The following constants specify the file/function combination
# to use to get the 'saving' and 'processing' statistics output.
# They may need to be changed if the codebase changes, and possibly add a line
# number to distinguish identically named functions in a file.

RE_TOP_LEVEL_FILE = re.compile('.*/bgd/framework\.py$')
RE_SAVING_FILE = re.compile('.*/iris/io/__init__\.py$')
RE_LOADING_FILE = re.compile('.*/bgd/plugins\.py$')

TOP_LEVEL_FUNC = '_process_single_level'
SAVING_FUNC = 'save'
LOADING_FUNC = '_touch_data'

SHOW_PROGRESS = True

COLUMN_HEADERS = ['Total Elapsed', 'Saving', 'Loading', ' Other']

RunStats = namedtuple('RunStats', ['mean', 'median', 'min', 'max'])


def get_matching_function_data(data_dict, re_file, function, filename,
                               warn=False):
    """
    data_dict: A dictionary with keys of the form:
        (filename, lineno, function name)

    Returns:
        The value with key matching `re_file` and `function`, or None if no
        function is matched. If more than one function is matched, raises an
        exception.
    """
    values = [value for (name, _, func), value in
              data_dict.iteritems() if re_file.match(name) and
              func == function]

    if len(values) > 1:
        raise ValueError("More than one {!r} function found".format(function))
    if len(values) == 0:
        if warn:
            warnings.warn("{}: no {!r} function found".format(filename,
                                                              function))
        return None

    return values[0]


def get_top_level_stats(all_calles_dict, re_file, function, filename,
                        warn=False):
    """
    all_calles_dict: A dictionary in the form of the pstats.Stats.all_callees
        attribute, containing time data for calls to or from functions
        identified by the keys.

        The keys are tuples of the form (filename, lineno, function name)
        and the values are dictionaries, with keys of the same form and
        values of  the form:
        (primitive calls, recursive calls, tottime, cumtime).

    Returns:
        A dictionary containing time statistcs for function calls made within
        `function`. If more than one matching function is detected an exception
        is raised. If no function is detected, returns an empty dictionary
    """
    callee_dict = get_matching_function_data(all_calles_dict, re_file,
                                             function, filename, False)
    if callee_dict is None:
        callee_dict = {}

    return callee_dict


def get_func_stats(data_dict, re_file, function, filename, warn=False):
    """
    data_dict: A dictionary in the form of the `pstats.Stats.stats`
        attribute, or a value from the `all_callees` attribute, containing
        time data for calls to or from functions identified by the keys.

        The keys are tuples of the form (filename, lineno, function name)
        and the values are tuples of the form
        (primitive calls, recursive calls, tottime, cumtime, dict) or
        (primitive calls, recursive calls, tottime, cumtime).
    Returns:
        (primitive calls, recursive calls, tottime, cumtime) for the specified
        function. If more than one matching function is detected an exception
        is raised. If no function is detected, returns zeroes
    """
    times = get_matching_function_data(data_dict, re_file, function, filename,
                                       warn)
    if times is None:
        times = (0, 0, 0, 0)

    # `times` is a tuple with 4 or 5 elements; return the first 4
    return times[:4]


def get_time_data(stat, warn=False):
    stat.calc_callees()
    top_level_stats = get_top_level_stats(stat.all_callees, RE_TOP_LEVEL_FILE,
                                          TOP_LEVEL_FUNC, stat.files[0], warn)
    _, _, _, saving_time = get_func_stats(top_level_stats, RE_SAVING_FILE,
                                          SAVING_FUNC, stat.files[0], warn)
    _, _, _, loading_time = get_func_stats(stat.stats, RE_LOADING_FILE,
                                           LOADING_FUNC, stat.files[0], warn)
    other_time = stat.total_tt - saving_time - loading_time
    return (saving_time, loading_time, other_time)


def make_stats(i, fname, nfiles):
    if SHOW_PROGRESS:
        print('\r', end='')
        print('Current dir: {}/{}'.format(i + 1, nfiles), end='')
        sys.stdout.flush()
    return pstats.Stats(fname)


def calc_stats(profile_files, times, warn=False):
    """Returns a tuple containing the mean, median, min and max time spent
       in each of total, saving, loading, processing and other"""
    nfiles = len(profile_files)
    stats = (make_stats(i, f, nfiles) for i, f in enumerate(profile_files))

    func_times = [get_time_data(stat, warn) for stat in stats]
    elapsed_times = [time.elapsed for time in times]

    if SHOW_PROGRESS:
        print()

    if len(func_times) == 0:
        fmeans = fmedians = fmins = fmaxes = [np.nan, np.nan, np.nan, np.nan]
    else:
        fmeans = np.mean(func_times, axis=0)
        fmedians = np.median(func_times, axis=0)
        fmins = np.min(func_times, axis=0)
        fmaxes = np.max(func_times, axis=0)

    if len(elapsed_times) == 0:
        emean = emedian = emin = emax = np.nan
    else:
        emean = np.mean(elapsed_times)
        emedian = np.median(elapsed_times)
        emin = np.min(elapsed_times)
        emax = np.max(elapsed_times)

    means = np.append([emean], fmeans)
    medians = np.append([emedian], fmedians)
    mins = np.append([emin], fmins)
    maxes = np.append([emax], fmaxes)

    return [RunStats(mean, median, mn, mx) for mean, median, mn, mx in
            zip(means, medians, mins, maxes)]


def profile_files_in(d):
    return glob.glob(os.path.join(d, '*.pstat'))


def log_files_in(d):
    return glob.glob(os.path.join(d, 'bunch.*.err'))


def calc_run_data(run_names, log_dirs, profile_dirs, combine=False,
                  warn=False):
    ndirs = len(log_dirs)
    if SHOW_PROGRESS:
        print('Total dirs:  {}'.format(ndirs))

    log_file_groups = [log_files_in(d) for d in log_dirs]
    profile_file_groups = [profile_files_in(d) for d in profile_dirs]
    if combine:
        # Combine groups of files into one
        log_file_groups = [list(chain.from_iterable(log_file_groups))]
        profile_file_groups = [list(chain.from_iterable(profile_file_groups))]

    ngroups = len(log_file_groups)
    time_details = []
    for i, file_group in enumerate(log_file_groups):
        if SHOW_PROGRESS:
            print('\r', end='')
            print('Processing log file group {}/{}'.format(i + 1, ngroups),
                  end='')
            sys.stdout.flush()
        time_details.append(log_parse.load_details(file_group,
                                                   strace=False,
                                                   last_two_lines=True))
    if SHOW_PROGRESS:
        print()

    times = [dets.itervalues() for dets in time_details]
    tot = sum(len(profile_files_in(d)) for d in profile_dirs)

    if SHOW_PROGRESS:
        print('Total profile files: {}'.format(tot))

    result = {run_name: calc_stats(file_group, time, warn)
              for run_name, file_group, time in
              zip(run_names, profile_file_groups, times)}
    return result


def print_stat(run_stats_list, stat_selector, out_file):
    column_widths = [len(header)+1 for header in COLUMN_HEADERS]
    stats = (stat_selector(run_stats) for run_stats in run_stats_list)
    for (stat, width) in zip(stats, column_widths):
        fmt = '{{0:{}.3f}}'.format(width)
        print(fmt.format(stat), file=out_file, end=' ')
    print(file=out_file)


def print_results(data, out_file):
    for run_name, run_stats_list in data.iteritems():
        print(run_name, file=out_file)
        print('       ', '  '.join(COLUMN_HEADERS), file=out_file)
        print('Mean   ', end='', file=out_file)
        print_stat(run_stats_list, lambda s: s.mean, out_file)
        print('Median ', end='', file=out_file)
        print_stat(run_stats_list, lambda s: s.median, out_file)
        print('Min    ', end='', file=out_file)
        print_stat(run_stats_list, lambda s: s.min, out_file)
        print('Max    ', end='', file=out_file)
        print_stat(run_stats_list, lambda s: s.max, out_file)
        print(file=out_file)


def explain():
    print("'Total Elapsed' is the total time taken by the process, as "
          "measured by the Unix 'time' command. The remaining columns "
          "represent time spent profiling, and therefore their sum is NOT "
          "equal to 'Total Elapsed'.")
    print()
    print("'Saving' is the total time spent in the function '{save}' in a "
          "file matching '{re_saving_file}'."
          .format(save=SAVING_FUNC, re_saving_file=RE_SAVING_FILE.pattern))
    print()
    print("'Loading' is the total time spent in the function '{load}' in a "
          "file matching '{re_loading_file}'. It is intended that this "
          "function should 'touch' cube data, in order to trigger loading."
          .format(load=LOADING_FUNC, re_loading_file=RE_LOADING_FILE.pattern))
    print()
    print("'Other' is other profiled time, which in most cases will consist "
          "of time processing plugins.")
    print()
    print("Each column contains the mean, median, minimum and maximum "
          "time for each Rose Bunch task.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Show time statistics of profile output.')
    parser.add_argument('-p', '--profile_dirs', metavar='dir', type=str,
                        nargs='+',
                        help='A list of directories containing output files '
                             'from the Python profiler.')
    parser.add_argument('-l', '--log_dirs', metavar='dir', type=str,
                        nargs='+',
                        help='A list of directories containing output from '
                             'the GNU version of the Unix time command.')
    parser.add_argument('-m', '--ignore_mismatch',
                        action="store_true",
                        help='Process the files even if there is a mismatch '
                             'between the number of log and profile files in '
                             'each directory.')
    parser.add_argument('-w', '--warn_missing',
                        action="store_true",
                        help='Show warnings about undetected functions.')
    parser.add_argument('-c', '--combine',
                        action="store_true",
                        help='Combine the data into a single table.')
    parser.add_argument('-o', '--output', metavar='file',
                        nargs=1, required=False,
                        help='The output file name. Optional. If not supplied '
                             'output will be written to stdout. If the file '
                             'exists it will be overwritten.')
    args = parser.parse_args()

    if args.profile_dirs is None:
        # Apply some default directory settings.
        run_names = [
            '0621T00Z_xce_single',
            '0621T00Z_xcf_single',
            '0621T00Z_xce_concurrent',
            '0621T00Z_xcf_concurrent']

        base_dir = '/data/users/dkirkham/shared/stage_profiles/'
        dirnames = [os.path.join(base_dir, name) for name in run_names]
        args.profile_dirs = [os.path.join(dirname, 'stats')
                             for dirname in dirnames]
        args.log_dirs = [os.path.join(dirname, 'times')
                         for dirname in dirnames]
    else:
        # Check that the two lists are the same length
        if len(args.profile_dirs) != len(args.log_dirs):
            sys.stderr.write(
                'Need the same number of profile dirs as log dirs!\n')
            sys.exit(2)
        if args.combine:
            run_names = ['Results']
        else:
            run_names = args.profile_dirs

    if not args.ignore_mismatch:
        # Check that each pair of directories has the same number of relevent
        # files
        mismatch = False
        for p_dir, l_dir in zip(args.profile_dirs, args.log_dirs):
            npf = len(profile_files_in(p_dir))
            nlf = len(log_files_in(l_dir))
            if npf != nlf:
                mismatch = True
                msg = 'Mismatched numbers of files in profile dir {!r} and '\
                      'log dir {!r}:\n'\
                      'Profile files: {}\n'\
                      'Log files: {}\n'
                sys.stderr.write(msg.format(p_dir, l_dir, npf, nlf))
        if mismatch:
            sys.stderr.write('Use -m or --ignore_mismatch to process the '
                             'files anyway.\n')
            sys.exit(3)

    data = calc_run_data(run_names, args.log_dirs,
                         profile_dirs=args.profile_dirs,
                         combine=args.combine,
                         warn=args.warn_missing)

    out_file = sys.stdout if args.output is None else open(args.output[0], 'w')
    print_results(data, out_file)

    if args.output is None:
        explain()
