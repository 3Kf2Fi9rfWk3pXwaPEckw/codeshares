"""
Work out which stashes appear when in pa+pb gpcs streams.

For global frozen processing.

"""

import os
import pickle
from glob import glob

from decoupler_shared.ff_phenom_content_summary import *

# Go to Bill's full-run snapshot directory.
start_dirpath = os.getcwd()
os.chdir('/research/data/d03/itwl/decoupler/data/global/20151019T1200Z/glm_um')

# Checkout all the filepaths (could be generated instead).
all_pa_filepaths = sorted(glob('umglaa_pa*.gpcs'))
all_pb_filepaths = sorted(glob('umglaa_pb*.gpcs'))

# Get phenomenon codes (and occurrence numbers) for each file.
pas_codes = {fn: scan_codes(fn) for fn in all_pa_filepaths}
pbs_codes = {fn: scan_codes(fn) for fn in all_pb_filepaths}

# Make sets of the codes appearing in each timestep file.
pa_code_sets = {fn: set(code[0] for code in code_and_num)
           for fn, code_and_num in pas_codes.items()}
pb_code_sets = {fn: set(code[0] for code in code_and_num)
           for fn, code_and_num in pbs_codes.items()}

# Make combined sets of all codes in each stream.
pb_allcodes = reduce(set.__or__, pb_code_sets.values())
pa_allcodes = reduce(set.__or__, pa_code_sets.values())

print 'CODES COMMON to "pa" and "pb":'
common_codes = pa_allcodes & pb_allcodes
print common_codes

print
print 'OUTPUT STEPS FOR EACH, "pb"'
pb_steps = {}
for code in pb_allcodes:
    dots = ''.join(['x' if code in pb_code_sets[fn] else '.'
                    for fn in all_pb_filepaths])
    print '{:30s} : {}'.format(str(code), dots)
    pb_steps[code] = dots

print
print 'PATTERNS for "pb" ...'
pb_patterns = set(pb_steps.values())
pb_patterns_and_codes = {pattern:set(code for code in pb_allcodes
                                     if pb_steps[code] == pattern)
                         for pattern in pb_patterns}
print '\n'.join(str(xx) for xx in pb_patterns_and_codes.items())

print
print 'OUTPUT STEPS FOR EACH, "pa"'
pa_steps = {}
for code in pa_allcodes:
    dots = ''.join(['x' if code in pa_code_sets[fn] else '.'
                    for fn in all_pa_filepaths])
    print '{:30s} : {}'.format(str(code), dots)
    pa_steps[code] = dots

print
print 'PATTERNS for "pa" ...'
pa_patterns = set(pa_steps.values())
pa_patterns_and_codes = {pattern:set(code for code in pa_allcodes
                                     if pa_steps[code] == pattern)
                         for pattern in pa_patterns}
print '\n'.join(str(xx) for xx in pa_patterns_and_codes.items())

# Save analysis results in run directory.
os.chdir(start_dirpath)

with open('pa_patterns_and_codes.pkl', 'wb') as fo:
    pickle.dump(pa_patterns_and_codes, fo)

with open('pb_patterns_and_codes.pkl', 'wb') as fo:
    pickle.dump(pb_patterns_and_codes, fo)
