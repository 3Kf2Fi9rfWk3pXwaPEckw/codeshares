import numpy as np
from string import lowercase as alphabet

#--TIME CONVERSION
#--Returns time input in hours as seconds
def in_seconds(current_time):
    return current_time*3600


#--CELL METHOD TESTS
#--Boolean test for cell method type maximum
def is_maximum_cell_method(input_method):
    if 'maximum' in input_method:
        return True
    return False


#--Boolean test for cell method type minimum
def is_minimum_cell_method(input_method):
    if 'minimum' in input_method:
        return True
    return False


#--Boolean test for cell method type mean
def is_mean_cell_method(input_method):
    if 'mean' in input_method:
        return True
    return False


#--Boolean test for no cell method type
def no_cell_method(input_method):
    if not input_method:
        return True
    return False


def get_cube_dim_coord_names(cube):
    return [coord.name() for coord in cube.coords(dim_coords=True)]


def decimate(data, step):
    data = data[..., ::step, ::step]
    return data


def locate_differences(ff, other, o_type, result_file):
    difference_indices = np.where(abs(ff - other) > 0.)

    with open(result_file, "a") as f:
        f.write('\nThe values that differ are...')
        str_ijkl = alphabet[8:]
        title_out = '\nFields file   {: >7} file  ({})\n'.format(o_type, ",".join(list(str_ijkl[:ff.ndim])))
        f.write(title_out)
        f.write('-' * len(title_out))
        for i, (ff_val, oth_val) in enumerate(zip(ff[difference_indices], other[difference_indices])):
            if i < 20:
                f.write('\n{: >12.2f}  {: >12.2f}  {}'.format(ff_val, oth_val, zip(*difference_indices)[i]))
            else:
                f.write('\nThere are differences in {} data points of {}.'.format(difference_indices[0].size, ff.size))
                f.write('  The first 20 were printed.')
                break


def percentage_difference(a,b):
    if a == 0 and b == 0:
        return 0
    if a + b == 0 and not a - b == 0:
        return abs(a-b)/abs(a)
    return abs((a-b)/(0.5*(a+b)))*100.


def print_header(result_file, comparison_type, run_id, run_time, ff_path, nc_path):
    with open(result_file, "w") as f:
        f.write('Testing for differences between fields files and {} files \n'.format(comparison_type))
        f.write('Run date/time : {}\n'.format(run_time))
        f.write('Model ID : {}\n'.format(run_id))
        f.write('FF input from : {}\n'.format(ff_path))
        f.write('{} input from : {}\n'.format(comparison_type, nc_path))
        f.write('================================================= \n')


def print_current_file(result_file, file_name, file_type='Fields'):
    with open(result_file, "a") as f:
        title_out = '\nOpened {} file : {}\n'.format(file_type, file_name)
        f.write('\n')
        f.write('=' * len(title_out))
        f.write(title_out)
        f.write('=' * len(title_out))

def print_summary(result_file, fail_count):
    with open(result_file, "a") as f:
        if fail_count == 0:
            title_out = '\nALL FILES MATCH\n'
        else:
            title_out = '\nTOTAL NUMBER OF FILES THAT DO NOT MATCH = {}\n'.format(fail_count)
        f.write('\n')
        f.write('=' * len(title_out))
        f.write(title_out)
        f.write('=' * len(title_out))


def get_cell_constraint(nc_cell_method):
    import iris
    if nc_cell_method:
        if 'maximum' in nc_cell_method:
            cell_method_constraint = iris.Constraint(cube_func=lambda cell: \
                                                         is_maximum_cell_method(str(cell.cell_methods)))
        elif 'minimum' in nc_cell_method:
            cell_method_constraint = iris.Constraint(cube_func=lambda cell: \
                                                         is_minimum_cell_method(str(cell.cell_methods)))
        elif 'mean' in nc_cell_method:
            cell_method_constraint = iris.Constraint(cube_func=lambda cell: \
                                                         is_mean_cell_method(str(cell.cell_methods)))
    else:
        cell_method_constraint = iris.Constraint(cube_func=lambda cell: \
                                                         no_cell_method(cell.cell_methods))
    return cell_method_constraint
        

def get_statistics(data_in):
    max_val = data_in.max()
    min_val = data_in.min()
    stdev_val = data_in.std()
    return max_val, min_val, stdev_val
    

def compare_statistics(ff_data, other_data, tolerance):
    #--Calculate statistics for matching fields file and NetCDF cube to compare values.
    ff_max_val, ff_min_val, ff_stdev_val = get_statistics(ff_data)
    nc_max_val, nc_min_val, nc_stdev_val = get_statistics(other_data)
    
    comp_max = percentage_difference(ff_max_val, nc_max_val)
    comp_min = percentage_difference(ff_min_val, nc_min_val)
    comp_stdev = percentage_difference(ff_stdev_val, nc_stdev_val)

    return comp_max <= tolerance, comp_min <= tolerance, comp_stdev <= tolerance


def print_comparison_success(result_file, diagnostic_name, comp_time, stash_code, non_ff_file, comparison_type):
    with open(result_file, "a") as f:
        f.write('\nChecking diagnostic : {} at time {} hours\n'.format(diagnostic_name, comp_time))
        f.write('STASH code : {}\n'.format(stash_code))
        f.write('{} file : {}\n'.format(comparison_type, non_ff_file))
        f.write('\t ***All statistics match, diagnostic equivalent*** \n')

	
def print_comparison_fail(result_file, diagnostic_name, comp_time, stash_code, dim_coord_names, ff_stats, other_stats):
    with open(result_file, "a") as f:
        f.write('\n\nDATA VALUES DO NOT MATCH \n')
        f.write('\t Diagnostic name : {}\n'.format(diagnostic_name))
        f.write('\t Time                        : {}\n'.format(comp_time))
        f.write('\t STASH CODE                  : {}\n'.format(stash_code))
        f.write('\t Dimensions compared (i,j..) : {}\n'.format(dim_coord_names))
        f.write('Values are:\n')
        f.write('Fields file:  Min = {: <15} Max = {: <15} Std.Dev = {: <15}\n'.format(ff_stats[1], ff_stats[0], ff_stats[2]))
        f.write('npz file:     Min = {: <15} Max = {: <15} Std.Dev = {: <15}\n'.format(other_stats[1], other_stats[0], other_stats[2]))
