import numpy as np


def shave(data, precision):
    """
    Perform bit-shaving on IEEE 754-2008 32-bit or 64-bit
    floating-point values.

    Preserves the most significant binary precision bits of the
    floating-point fraction, and zeros all other less significant
    bits.

    Parameters
    ----------
    data: array
        Floating-point arry of 32-bit or 64-bit values to be bit-shaved.
    precision: integer
        The non-negative integer specifying the number of most significant
        floating-point fractional bits to preserve.

    Returns
    -------
    A new array appropriately bit-shaved.

    """
    dtype = data.dtype
    if np.issubdtype(dtype, np.float32):
        # IEEE 754-2008 binary32 format.
        N, ones, utype, uint = 23, 0xffffffff, 'u4', np.uint32
    elif np.issubdtype(dtype, np.float64):
        # IEEE 754-2008 binary64 format.
        N, ones, utype, uint = 52, 0xffffffffffffffff, 'u8', np.uint64
    else:
        emsg = 'Expected float32 or float64 dtype, got {}'
        raise TypeError(emsg.format(dtype))

    if precision < 0:
        emsg = 'Expected non-negative fractional precision range for {} is ' \
            '0-{}, got precision {}'
        raise ValueError(emsg.format(dtype, N, precision))

    mask = (ones << (N - precision)) & ones if precision < N else ones
    result = np.bitwise_and(data.view(dtype=utype), uint(mask)).view(dtype)

    return result
