"""
Scan a fieldsfile and report on data range and BACC value for each 'code'.
In this case, a 'code' includes lbproc.

"""

from collections import namedtuple

import numpy as np

from iris.experimental.um import FieldsFileVariant

FldInfo = namedtuple('FldInfo', ['i', 'bacc', 'dmin', 'dmax', 'nmdi', 'lbpack'])

def scan_ff(filepath, only_codes_u7_u4_lbproc=None):
    """
    Report packing info for a fieldsfile.

    Args:

    * filepath (string):
        path to fieldsfile variant to open + scan.
    * only_codes_u7_u4_lbproc (list of (int, int, int)):
        codes to restrict report to = (lbuser7, lbuser4, lbproc).
        If not given, report on all found in file.

    """
    ffv = FieldsFileVariant(filepath, FieldsFileVariant.READ_MODE)
    fields = [fld for fld in ffv.fields if hasattr(fld, 'lbvc')]
    id_codes = zip([fld.lbuser7 for fld in fields],
                   [fld.lbuser4 for fld in fields],
                   [fld.lbproc for fld in fields])
    unique_codes = sorted(set(id_codes))
    print 'FILE : {}'.format(filepath)
    print '  n total fields = {:d}'.format(len(fields))
    print '  n unique codes = {:d}'.format(len(unique_codes))
    print 'Packing info by codes:'

    if only_codes_u7_u4_lbproc is not None:
        # restrict to specific codes (NB and report when not present !)
        unique_codes = only_codes_u7_u4_lbproc

    line_sample = \
        'CODE 07.00004/12345 *nnnn @nnnn-nnnn : 0000000/0000000 =000.0% | 000.0 |  00001 | 00000000.000 - 00000000.000 | 00000000.000 - 00000000.000 |'
    heading_string = \
        ' CODE usr7.4[lbproc] : field numbers    | mdi/total size  =%      | BACC  | LBPACK | MIN VAL  min - max          | MAX VAL  min - max          |'
    line_format_ok = (
        '     {:02d}.{:05d}[{:05d}] :'
        ' *{:04d} @{:04d}-{:04d} |'
        ' {:07d}/{:07d} ={:05.1f}% |'
        ' {:05.1f} |'
        '  {:05d} |'
        ' {:12.30f} - {:12.30f} |'
        ' {:12.30f} - {:12.30f} |'
    )
    line_sample = \
        'CODE 07.00004/12345 *nnnn @nnnn-nnnn : 0000000/0000000 =000.0% | 000.0 |  00001 | 00000000.000 - 00000000.000 | 00000000.000 - 00000000.000 |'
    line_format_none = \
        '     {:02d}.{:05d}[{:05d}] :   --no fields--'
    print heading_string

    for user7, user4, lbproc in unique_codes:
        fld_mdi = None
        fld_size_tote = None
        fld_lbpack = None
        flds_info = []
        for i_fld, fld in enumerate(fields):
            if (fld.lbuser4 == user4
                and fld.lbuser7 == user7
                and fld.lbproc == lbproc):
                    bacc = fld.bacc
                    data = fld.get_data()
                    if fld_mdi is None:
                        fld_mdi = fld.bmdi
                    else:
                        assert fld_mdi == fld.bmdi
                    if fld_size_tote is None:
                        fld_size_tote = data.size
                    else:
                        assert fld_size_tote == data.size
                    if fld_lbpack is None:
                        fld_lbpack = fld.lbpack
                    else:
                        assert fld_lbpack == fld.lbpack
                    n_mdi = np.count_nonzero(data == fld_mdi)
                    dmin = min(data[data != fld_mdi])
                    dmax = max(data[data != fld_mdi])
                    flds_info.append(FldInfo(
                        i=i_fld, bacc=bacc, dmin=dmin, dmax=dmax,
                        nmdi=n_mdi, lbpack=fld_lbpack))
        n_flds = len(flds_info)
        if n_flds < 1:
            print line_format_none.format(user7, user4, lbproc)
        else:
            i_min = min(info.i for info in flds_info)
            i_max = max(info.i for info in flds_info)
            dmin_min = min(info.dmin for info in flds_info)
            dmin_max = max(info.dmin for info in flds_info)
            dmax_min = min(info.dmax for info in flds_info)
            dmax_max = max(info.dmax for info in flds_info)
            nmdi_min = min(info.nmdi for info in flds_info)
            nmdi_max = max(info.nmdi for info in flds_info)
            bacc_min = min(info.bacc for info in flds_info)
            bacc_max = max(info.bacc for info in flds_info)
            assert nmdi_min == nmdi_max, 'N-MDI varies: min={} max={}'.format(
                nmdi_min, nmdi_max)
            nmdi = nmdi_min
            pc_mdi = (100.0 * nmdi) / fld_size_tote
            assert bacc_min == bacc_max, 'BACC varies: min={} max={}'.format(
                bacc_min, bacc_max)
            bacc = bacc_min
            print line_format_ok.format(
                user7, user4, lbproc,
                n_flds, i_min, i_max,
                nmdi, fld_size_tote, pc_mdi, bacc,
                fld_lbpack,
                dmin_min, dmin_max, dmax_min, dmax_max)
    ffv.close()


# Default testfiles on HPC
# As specified in Laura's config "global-frozen.py"
#    INPUT_PATHS = {
#        'umglaa_pa006.calc': '/data/d04/ithr/decoupler/global/2015051300/umglaa_pa006.calc',
#        'umglaa_pb006': '/data/d04/ithr/decoupler/global/2015051300/umglaa_pb006',
#    }
DEFAULT_SCAN_PATHS = [
    '/data/d04/ithr/decoupler/global/2015051300/umglaa_pa006.calc',
    '/data/d04/ithr/decoupler/global/2015051300/umglaa_pb006'
]


# Default 'sample' stashes
# As specified in Laura's config "global-frozen.py"
#    '1_12_0': StructuredFFLoad('umglaa_pb006', 'm01s00i012'),
#    '1_16004_0': StructuredFFLoad('umglaa_pb006', 'm01s16i004'),
#    '1_16222_0': StructuredFFLoad('umglaa_pa006.calc', 'm01s16i222'),
#    '1_254_0': StructuredFFLoad('umglaa_pb006', 'm01s00i254'),
#    '1_266_0': StructuredFFLoad('umglaa_pb006', 'm01s00i266'),
#    '1_2_0': StructuredFFLoad('umglaa_pb006', 'm01s00i002'),
#    '1_3236_0': StructuredFFLoad('umglaa_pa006.calc', 'm01s03i236'),
#    '1_3281_0': StructuredFFLoad('umglaa_pa006.calc', 'm01s03i281'),
#    '1_408_0': StructuredFFLoad('umglaa_pb006', 'm01s00i408'),
#    '1_4203_0': StructuredFFLoad('umglaa_pa006.calc', 'm01s04i203'),
DEFAULT_SCAN_CODES = [
    (1, 4203, 0),  # single-level
    (1, 3236, 0),  # single-level
    (1, 16222, 0), # single-level
    (1, 3281, 0),  # single-level

    (1, 266, 0),   # multi-level
    (1, 408, 0),   # multi-level
    (1, 16004, 0), # multi-level
    (1, 2, 0),     # multi-level
    (1, 254, 0),   # multi-level
    (1, 12, 0),    # multi-level

]


def allcodes(filepath_or_ffv):
    """
    Return sorted unique stashes (user7, user5, lbproc) for a fieldsfile.

    Pass it either an open file or a path.

    """
    opens_file = not hasattr(filepath_or_ffv, 'fields')
    if opens_file:
        ffv = FieldsFileVariant(filepath_or_ffv, FieldsFileVariant.READ_MODE)
    else:
        ffv = filepath_or_ffv

    try:
        fields = [fld for fld in ffv.fields if hasattr(fld, 'lbvc')]
        id_codes = zip([fld.lbuser7 for fld in fields],
                       [fld.lbuser4 for fld in fields],
                       [fld.lbproc for fld in fields])
        unique_codes = sorted(set(id_codes))
    finally:
        if opens_file:
            ffv.close()

    return unique_codes


def scan_ff_sample_fields(filepaths=None, fieldcodes=None):
    """Scan default codes in default (test) files."""
    if filepaths is None:
        filepaths = DEFAULT_SCAN_PATHS
    if fieldcodes is None:
        fieldcodes = DEFAULT_SCAN_CODES
    for filepath in filepaths:
        print
        scan_ff(filepath, fieldcodes)


if __name__ == '__main__':
#    # Test form, only works on desktop.
#    in_path = '/data/local/itpp/decoupler/sample_model_data/global/2015-01-20/umglaa_pa150'
#    scan_ff(in_path, DEFAULT_SCAN_CODES)

    # Default operation : report on reference files.
    scan_ff_sample_fields()
