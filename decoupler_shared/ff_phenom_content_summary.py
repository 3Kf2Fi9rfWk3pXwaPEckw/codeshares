#!/usr/bin/env python2.7
#from decoupler_shared.fields_accuracy_info import allcodes

from glob import glob
import os
import os.path
import sys

from iris.experimental.um import FieldsFileVariant


def scan_codes(filepath_or_ffv):
    """
    Return sorted unique stashes (user7, user5, lbproc) for a fieldsfile,
    combined with the total number of matching fields in the file.

    Pass it either an open file or a path.

    """
    opens_file = not hasattr(filepath_or_ffv, 'fields')
    if opens_file:
        ffv = FieldsFileVariant(filepath_or_ffv, FieldsFileVariant.READ_MODE)
    else:
        ffv = filepath_or_ffv

    try:
        fields = [fld for fld in ffv.fields if hasattr(fld, 'lbvc')]
        id_codes = zip([fld.lbuser7 for fld in fields],
                       [fld.lbuser4 for fld in fields],
                       [fld.lbproc for fld in fields])
        unique_codes = sorted(set(id_codes))
    finally:
        if opens_file:
            ffv.close()

    # pair each id with a count of how many fields have it.
    codes_info = [(this_code,
                   sum(id_code == this_code for id_code in id_codes))
                  for this_code in unique_codes]
    return codes_info


def show_all_diagnostic_codes(filepath):
    print 'ALL PHENOM CODES FOR "', filepath
    codes_and_numbers = scan_codes(filepath)
    fmt_head = '####: _s _____i _lbproc  : n-fields'
    print fmt_head
    fmt_body = '{:04d}: {:2d} {:6d} {:07d}  : {:6d}'
    for i_code, code in enumerate(codes_and_numbers):
        ((i_s, i_i, lbproc), n_fields) = code
        print fmt_body.format(i_code, i_s, i_i, lbproc, n_fields)


STREAM_EXCLUDE_DEFAULTS = ['umgl.pp8']
# NOTE: patterns might be more useful, but doesn't seem needed for now.

def list_candidate_stream_files(dirpath='.',
                         exclude_names=STREAM_EXCLUDE_DEFAULTS):
    """Return a list of possible stream files in a run directory."""
    start_dir = os.getcwd()
    try:
        os.chdir(dirpath)
        tailchars = '.done'
        n_tail = len(tailchars)
        selected_filenames = []
        done_files = glob('*{}'.format(tailchars))
        for done_filename in done_files:
            assert done_filename[-n_tail:] == tailchars, "? {!r} => {!r} =!= {!r}".format(
                done_filename, done_filename[-n_tail:], tailchars)
            poss_filename = done_filename[:-n_tail]
            calc_filename = poss_filename + '.calc'
            if os.path.exists(calc_filename):
                poss_filename = calc_filename
            if os.path.exists(poss_filename):
                selected_filenames.append(poss_filename)
    finally:
        os.chdir(start_dir)

    # exclude the unwanted ones + return sorted list.
    selected_filenames = set(selected_filenames) - set(exclude_names)
    selected_filenames = sorted(selected_filenames)
    return selected_filenames


def print_on_lines(vec):
    return '\n'.join(str(xx) for xx in vec)


# Use the following base directory to access a run
RUN_BASEDIR = '/critical/fpos2/cylc-run/mi-af228/share/data'
DEFAULT_RUN_TIMEDIR = '20151019T1200Z'
RUN_TYPEDIR = 'glm_um'

if __name__ == '__main__':
    # Define the run basetime as another path component
    if len(sys.argv) < 2:
        test_run_timedir = DEFAULT_RUN_TIMEDIR
    else:
        test_run_timedir = sys.argv[1]
    # Construct the base pathname for the global model outputs.
    test_rundir_path = os.path.join(RUN_BASEDIR,
                                    test_run_timedir,
                                    RUN_TYPEDIR)

    print
    print 'SCANNING RUN DIRECTORY: ', test_rundir_path

    # Get potential stream-file names.
    candidate_files = list_candidate_stream_files(test_rundir_path)
    print
    print 'LIST OF POTENTIAL STREAM FILES ...'
    print print_on_lines(candidate_files)
    print
