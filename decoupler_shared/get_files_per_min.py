# -*- coding: iso-8859-1 -*-
'''
A script that can be used to generate tables with files per minute generated
from a run of stage.

Counts the number of .nc files created in each minute for the duration of each
stage task in a cycle of either a dev or model suite.
Outputs the data in a table that can be copied straight into a Jira comment.
Can also be used to print arrays of the data for plotting with example code
available at the bottom.

This script should be run on the HPC, on the correct hall for your suite.
The input_directory should be set to the
equivalent path for your suite:
~user/cylc-run/<suite_name>/share/cycle/<cycle_time>/decoupler/glm_frozen_pcs
It will produce a separate table for each task subdirectory it finds within
this directory.
You should also specify a file_extension for the files you want to count. This
would normally be either '*.nc' or '*.pstat'.
'''

import os
import matplotlib.pyplot as plt
import subprocess
import glob


#Example directories: (data is not available on each hall)
# Note these paths should be absolute paths or relative to your current directory
# Using paths with environment variables in or ~ will not work at the moment.
# You also need to be one directory higher than the directory with the files
# you want to analyse. It is designed this way so that you can analyse the
# files created for more than one task using this script.

# Available on F
#input_directory = "/home/d02/frust/cylc-run/glm_frozen_file_timings/share/cycle/20160804T0000Z/decoupler/glm_frozen_pcs"
# Available on E
#input_directory = "/critical/fpos2/cylc-run/glm_frozen_ps38_profiling_dd483/share/cycle/20160729T0600Z/decoupler.saved/glm_frozen_pcs/"
input_directory = "/critical/fpos2/cylc-run/mi-al799_glm_frozen_ps38_profiling_dd483_2/share/data/20160808T0600Z/decoupler/glm_frozen_pcs/"

#Choose a file extension for globing files in a directory. Eg '*.pstat' or '*.nc'
file_extension = '*.nc'

def get_count(times):
    '''
    Returns a dictionary containing the time in MM:SS format and the number of
    times that time appears in the list of times taken as an argument.
    Also adds entries into the dictionary for any times that are missing
    between the earliest and latest times in the list of times.
    '''
    # Set up a dictionary to associate the unique timestamps
    # with the number of files with that time stamp
    dict1={}
    for time in set(times):
        cnt=0
        for t in times:
            if t==time:
                cnt=cnt+1
        dict1.update({time:cnt})

    # Set up some logic to add data to the dictionary for times between the
    # first and last timestamp where no files were created.
    first_time = sorted(dict1.keys())[0].split(':')
    last_time = sorted(dict1.keys())[-1].split(':')
    # Set up lists for the time in minutes since the first file was created (in
    # times) and the corresponding number of files created (in files) useful if
    # you want to plot the data.
    times=[]
    # If the first and last file were not created in the same hour create the
    # times from the first_hour:first_minute to first_hour:59 and from
    # last_hour:00 to last_hour:last_minute
    if first_time[0] != last_time[0]:
        times = ['{:0>2}:{:0>2}'.format(first_time[0], minute) for minute in range(int(first_time[1]), 60,1)]
        times = times + ['{:>2}:{:0>2}'.format(last_time[0], minute) for minute in range(0, int(last_time[1]),1)]
    # Else if the first and last file were created in the same hour then create
    # the times from first_hour:first_minute to first_hour:last_minute.
    else:
        times = ['{:0>2}:{:0>2}'.format(first_time[0], minute) for minute in range(int(first_time[1]), int(last_time[1]),1)]
    # Work out how many hours were from first hour to the last hour where files
    # were created.
    extra_hours = range(int(first_time[0])+1, int(last_time[0]))
    # If there is any extra hours create all the times in these hours
    # ie times from extra_hour:00 to extra_hour:59
    if len(extra_hours)>0:
        for hour in extra_hours:
            times=times + ['{:0>2}:{:0>2}'.format(hour, minute) for minute in range(0, 60,1)]
    # Now we have a list of all the possible time stamps that could occur
    # between the first and last time, check for any that are missing from the
    # dictionary and add in an entry that says no files were created in that
    # minute.
    for time in times:
        if time not in dict1.keys():
            dict1.update({time:0})
    return dict1

if __name__=="__main__":

    # Get all the different task directories
    directories = [os.path.join(input_directory,o) for o in os.listdir(input_directory) if os.path.isdir(os.path.join(input_directory,o))]

    # Process the files for each task separately
    for directory in directories:
        # Get the ls -l information for the netCDF files which contain a timestamp
        # for when the file was last changed. Check there are files matching the
        # file_extension provided, else skip this directory.
        if len(glob.glob(os.path.join(directory,file_extension)))>0:
            ls_output = subprocess.check_output(['ls', '-l'] + glob.glob(os.path.join(directory,file_extension)))
        else:
            print 'No files matching file extension {} in this directory: {}'.format(file_extension, directory)
            continue
        # Ignore the first line in the output from ls -l
        lines= ls_output.split('\n')[1:]
        # Get the date out of the output and set up a dictionary to store the times
        # for each date
        dates = set([line.split()[6] for line in lines if line is not ''])
        date_dict = {date:[] for date in dates}
        # Extract the time stamps from the ls_output, and store them in a list
        # associated with the relevant date.
        for line in lines:
            if line is not '':
                date = line.split()[6]
                time = line.split()[7]
                date_dict[date].append(time)
        # Replace the list of times with a dictionary, containing {time:count}
        # entries for each time, showing how many files were created at that time.
        for date in (dates):
            times=date_dict[date]
            date_dict[date]=get_count(times)
        # Print out the directory so the user knows which task the data is for
        print 'Directory analysed:'
        print directory

        t=[]
        files=[]
        # Sort the dictionary and print it in a way that is useful for a Jira table.
        # also populate the lists t and files which are useful for plotting.
        i=0
        # First sort the dates, assuming there are only two dates and dealing with
        # the case where it goes over month boundaries.
        if 1 in dates:
            sorted_dates=sorted(dates, reverse)
        else:
            sorted_dates=sorted(dates)
        # Then print the data in the correct order.
        for date in sorted_dates:
            dict1=date_dict[date]
            for key, value in sorted(dict1.items()):
                print '|{}|{}|'.format(key,value)
                t.append(i)
                files.append(value)
                i=i+1
        # Print minute and number of files
        print 'minutes and corresponding number of files in lists for plotting'
        print t
        print files
        print '\n'



        # Example code for plotting the files per minute:
        #plt.plot(t, files)
        #plt.xlabel('Time since start of run (mins)')
        #plt.ylabel('number of files produced (per min)')
        #plt.show()
