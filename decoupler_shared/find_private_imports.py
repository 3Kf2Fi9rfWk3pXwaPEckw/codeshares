# -*- coding: iso-8859-1 -*-
"""
Search recursively all Python files within a directory and locate all private
imports and uses of private methods within these Python files. 

Used for ticket DD-375

"""

import os
import re


# Regex needs to match the following:
#   * import <library>
#   * import <lib1>, <lib2>, ...
#   * from <lib> import <sub-lib>
#   * from <lib> import <sub-lib1>, <sub-lib2>, ...
#   * import <lib> as <name>
#   * from <lib> import <sublib> as <name>
#   * import <lib1>, <lib2>,\n <lib3>, ...
#
# This boils down to three different matches:
#   * ".* import <names> $"
#   * ".* as <names> $"
#   * pesky newlines:
#        * ".* import ( <names> ,$ .* )"
#        * ".* import \\ <names> $"

# A regular expression for finding any import statements within a python file
re_simple = r'.*import.*\.*'
# The complied regular expression
test1 = re.compile(re_simple)


def find_python_files(start_dir):
    """
    Recursively find all Python files within a specified directory `start_dir`
    and all sub-directories.
    
    Args:
    * start_dir (string)
       a string containing the starting directory for the search.

    """
    py_files = []
    extn = '.py'
    all_files_and_folders = os.walk(start_dir)
    for dir_path, _, dir_files in all_files_and_folders:
        for dir_file in dir_files:
            _, file_extn = os.path.splitext(dir_file)
            if file_extn == extn:
                py_files.append("/".join([dir_path, dir_file]))
    return py_files


def file_lines(filename):
    """
    Returns a list of with each line in the file as a separate string.

    Args:
    
    * filename (string):
        The path to a file. 

    """
    with open(filename, "r") as f:
        result = f.readlines()
    return result


def get_imports(file_lines):
    """
    Return a list of all libraries imported within a Python script.
    Considers the cases where import statements continue over multiple lines,
    either through the use of brackets or \ continuation. 
    Uses the global test1 regular expression defined above.
    
    Args:
    
    * file_lines (list):
        A list containing each line from a file in a separate string. 
    
    """
    imports = []
    continue_slash = False
    continue_bracket = False
    import_slash_temp_string=''
    import_bracket_temp_string=''
    for line_index, line in enumerate(file_lines):
        if continue_slash:
            import_slash_temp_string = import_slash_temp_string+file_lines[line_index]
            continue_slash = '\\' in file_lines[line_index]
            if not continue_slash:
                imports.append(import_slash_temp_string)
                import_slash_temp_string='' 
        elif continue_bracket:
             if ')' in file_lines[line_index]:
                  import_bracket_temp_string=import_bracket_temp_string+file_lines[line_index]
                  imports.append(import_bracket_temp_string)
                  import_bracket_temp_string=''
                  continue_bracket = False
             else:
                  import_bracket_temp_string=import_bracket_temp_string+file_lines[line_index]
        else:
            match = re.match(test1, line)
            if match is not None:
              import_statement = re.match(test1, line).group(0)
              if '\\' in import_statement:
                  import_slash_temp_string = import_slash_temp_string + import_statement
                  continue_slash = True
              elif '(' in import_statement:
                  if ')' in import_statement:
                      import_bracket_temp_string = import_bracket_temp_string +import_statement
                      imports.append(import_bracket_temp_string)
                      import_bracket_temp_string=''
                  else:
                      import_bracket_temp_string = import_bracket_temp_string +import_statement
                      continue_bracket = True
              else:
                  imports.append(import_statement)
    return imports


def find_libraries(import_statements):
    """    
    Returns a list of library names to look for private function usage.

    Args:
    
    * import_statements (list):
        A list of every import statement from within a python script. 

    """ 
    libs = []
    for import_statement in import_statements:
        if 'as' in import_statement:
            # Use renamed library
            libs.append(import_statement.rstrip('\n').rstrip(' ').split(' ')[-1].translate(None, '() '))
        elif '*' in import_statement:
            print import_statement
        elif '=' in import_statement:
            pass
        else:
            # Take all libraries after the import keyword
            import_index = import_statement.index("import")
            split_imports = import_statement[import_index+6:].split(",")
            for x in split_imports:            
                libs.append(x.translate(None, " \()").rstrip('\n').lstrip('\n'))
    return libs       


def get_private_function_use(file_lines, libraries):
    '''
    Returns a list all private function calls from the libraries within file_lines
    
    Args:
    
    * file_lines (list):
        A list containing each line from a file in a separate string. 
    * libraries (list):
        A list containing each library imported within the python script in a separate string.
    '''
    imports = []
    for lib in libraries:
        # A regular expression for '<lib>._*' where lib is a library from the list of libraries.
        # This is used to find any private function calls from this library.
        re_private = r'.*{}\._.*'.format(lib)
        reg_ex = re.compile(re_private)
        for line_index, line in enumerate(file_lines):
            match = re.match(reg_ex, line)
            if match is not None:
                imports.append('Library {}, Line number {}, private function {}'.format(lib, line_index, match.group(0)))
    return imports


def main():
    start_dir = '/home/h05/frust/git/bgd'
    py_files = find_python_files(start_dir)
    with open('bgd_private_function_use_new_lines.txt', 'w') as output:
      for f in py_files:
        output.write('Filename = {} \n'.format(f))
        output.write('Import statements in file:{} \n'.format(get_imports(file_lines(f))))
        libraries =  find_libraries(get_imports(file_lines(f)))
        output.write('Libraries being imported (including renamed imports): {} \n'.format(libraries))
        output.write('Private function use: {} \n'.format(get_private_function_use(file_lines(f), libraries)))
        output.write('\n')


if __name__ == '__main__':
    main()
