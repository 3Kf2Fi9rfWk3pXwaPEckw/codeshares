'''
Show that bitshaving WGDOS data to the 'stated accuracy' has no effect.
Test saving lossless/lossy at stated accuracy, and with added noise,
measuring load+save times and filesizes.

'''
import sys
import os
import os.path

import numpy as np
import iris

from pp_utils import TimedBlock

from decoupler_shared.bit import shave
import decoupler_shared.fields_accuracy_info as fai


in_dir = os.path.dirname(__file__)
if len(in_dir) == 0:
    in_dir = os.getcwd()

out_dir = os.sep.join((in_dir, 'bitshave_outputs'))
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# Load air_temperature data : 3 times * 70 levels = 210 fields
in_path = fai.DEFAULT_SCAN_PATHS[1]
if any(arg == 'TEST' for arg in sys.argv[1:]):
    in_path = os.path.join(in_dir, 'bitshave_test_data.nc')

test_cube = iris.load_cube(in_path, 'air_temperature')
if len(sys.argv) > 1 and str.isdigit(sys.argv[1]):
    n_levels = int(sys.argv[1])
    test_cube = test_cube[:, :n_levels]

print 'Basic data cube:'
print test_cube

# The data range is around 190.0 - 320.0 K
# So, we "should" bitshave this with log2(512.0/0.125) bits = 9+3 = 12 bits
data = test_cube.data
print
print 'data min, max = ', np.min(data), np.max(data)

# It happens that this has BACC=-3.0, so should all be multiples of 0.125.
granularity = 0.125
whole_data = data / granularity

# Calculate some random noise in the same shape.
size, shape = data.size, data.shape
noise = np.random.uniform(size=size).reshape(shape)
noise = np.asarray(noise, dtype=np.float32)
noise = granularity * (noise - 0.5)

# Check that ?
print
print 'Check array quantisation to multiples of 0.125 ...'
assert np.array_equal(whole_data, np.round(whole_data))
print '  ...done.'

print
print 'Check that shaving to 12bits does nothing...'
shaved_data_12 = shave(data, 12)
assert np.array_equal(shaved_data_12, data)
print ' ...done.'

print
print 'Check that shaving data to 10 bits does NOT do nothing...'
shaved_data_10 = shave(data, 10)
assert not np.array_equal(shaved_data_10, data)
print ' ...done.'

print
print 'Check that shaving data+NOISE to 18 bits does NOT do nothing...'
noisy  = data + noise
shaved_noisy_18 = shave(noisy, 18)
assert not np.array_equal(shaved_noisy_18, noisy)
print ' ...done.'

# The data range is about 146.250 - 315.125.
# Removing 190 reduces to about 0-168, so that should go in 8 bits (saving 1 ? not much!).
print
print 'Check that shaving (data-145.0) to *11* bits does nothing...'
reduced_data = data - 145.0
shaved_reduced_data_11 = shave(reduced_data, 11)
assert np.array_equal(shaved_reduced_data_11, reduced_data)
print ' ...done.'

# tests : (data, test-type-name, lossy-n-DP)
test_cases = ((data, 'CleanWgdos', 1),
              (noisy, 'Noisy', 3),
              (reduced_data, 'OffsetRemoved', 1))

save_cube = test_cube.copy()

def do_measures(data, describe, save_kwargs):
    save_filename = 'bitshave_test_save_{}.nc'.format(describe)
    save_filepath = os.path.join(out_dir, save_filename)
    save_cube.data = data
    with TimedBlock() as tb_save:
        iris.save(save_cube, save_filepath, **save_kwargs)
    with TimedBlock() as tb_load:
        readback_cube = iris.load_cube(save_filepath)
    with TimedBlock() as tb_data:
        readback_data = readback_cube.data
    errors = readback_data - data
    err_min, err_max, err_std = np.min(errors), np.max(errors), np.std(errors)
    print
    print 'Type {}, dtype = {} : file={}'.format(
        describe, data.dtype, save_filename)
    print '  Time to save :     {:7.5f} secs.'.format(tb_save.seconds())
    print '  Time to load :     {:7.5f} secs.'.format(tb_load.seconds())
    print '  Time to get data : {:7.5f} secs.'.format(tb_data.seconds())
    print '  Data errors : min={}, max={}, std={}'.format(
        err_min, err_max, err_std)
    print '  File size = {} Mb'.format(os.path.getsize(save_filepath) * 1.0e-6)

for trial in range(3):
    print
    print 'Save various versions with NO compression... : TRY#', trial
    for data, describe, ndps in test_cases:
        describe = 'Normal-{}'.format(describe)
        do_measures(data, describe, {})

for trial in range(3):
    print
    print
    print 'Save various versions with LOSSLESS compression... : TRY#', trial
    save_cube = test_cube.copy()
    for data, describe, ndps in test_cases:
        describe = 'Lossless-{}-N{}'.format(describe, trial)
        do_measures(data, describe, dict(zlib=True, complevel=1))

for trial in range(3):
    print
    print 'Save various versions with LOSSY compression... : TRY#', trial
    save_cube = test_cube.copy()
    for data, describe, ndps in test_cases:
        describe = 'Lossy-{}DP-{}'.format(ndps, describe)
        do_measures(data, describe, dict(zlib=True, least_significant_digit=ndps))
