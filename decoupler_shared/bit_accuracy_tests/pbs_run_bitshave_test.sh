#PBS -N bit_acc_single
#PBS -j oe
#PBS -l walltime=00:15:00
#PBS -l select=1
#PBS -q normal

export ENVIRON=~itpp/miniconda/envs/bgd/
export PATH=$ENVIRON/bin:$PATH
export PYTHONPATH=/small/home/d01/itpp/git/codeshares

#aprun -n 1 bash /small/home/d01/itpp/git/codeshares/decoupler_shared/bit_accuracy_tests/test_bitshave_accuracy_short.sh
aprun -n 1 bash /small/home/d01/itpp/git/codeshares/decoupler_shared/bit_accuracy_tests/test_bitshave_accuracy_full.sh
