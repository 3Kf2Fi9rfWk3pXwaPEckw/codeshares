# Short python script for creating a JIRA table.

import numpy as np
import os
import re


def print_jira_table():
    task_name = os.environ["task_name"]
    cycle_time = os.environ["cycle_time"]
    queued_time = os.environ["queued_time"]
    elapsed_time = os.environ["elapsed_time"]

    task_names = task_name.split("\n")
    task_names_padded = []
    for tn in task_names:
        number = re.findall(r'\d+', tn)[0]
        tn = tn.replace(number, number.zfill(3))
        task_names_padded.append(tn)

    sorting_index = np.argsort(task_names_padded)
    task_names_padded = np.array(task_names_padded)[sorting_index]
    cycle_times = np.array(cycle_time.split("\n"))[sorting_index]
    queued_times = np.array(queued_time.split("\n"))[sorting_index]
    elapsed_times = np.array(elapsed_time.split("\n"))[sorting_index]

    print "||Cycletime||Task name||PBS Duration||Compute Duration||"
    for ct, tn, qt, et in zip(cycle_times, task_names_padded,
                              queued_times, elapsed_times):
        new_tn = tn.strip()
        new_qt = qt.split(" ")[0]
        new_et = et.split(" ")[0]
        print "||{}||{}|{}|{}|".format(ct, new_tn, new_qt, new_et)

if __name__ == "__main__":
    print_jira_table()
