#!/bin/sh
#PBS -N m01s00i268
#PBS -j oe
#PBS -l walltime=02:00:00
#PBS -q shared
#PBS -l select=1:ncpus=1

PYTHON=/home/d03/itwl/miniconda/envs/bgd/bin/python
BOOST=/home/d03/itwl/projects/git/decoupler/ff_data_hires.py
IPATH=/data/d03/itwl/decoupler/compression/20150929T0000Z
IFILE=${IPATH}/umgl.pp8
STASH="m01s00i268"
OFILE=${WORKING}/decoupler/${STASH}.out
DFILE=${OFILE}.done

${PYTHON} ${BOOST} ${IFILE} ${STASH} 2560 1920 ${OFILE} --nz 140 

if [ ${?} -eq 0 ]
then
    mv ${OFILE} ${DFILE}
fi
