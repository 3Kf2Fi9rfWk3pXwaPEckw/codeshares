"""
Analyse "worst-case" (i.e. uncompressed) netCDF output sizes, for the
global-frozen diagnostics.

Derive a 'best-fit' linear approximation to file-total-bytes/field
- except for chunk t=000, where orography inclusion messes this up.
Show how close this approximation is.

Double-check that the list of output files matches all the expected code/chunk
combinations from the pattern analysis.

"""
import os
import os.path
import pickle

import netCDF4 as nc
import numpy as np
from scipy.stats import linregress

data_dir = '/data/d03/itwl/decoupler/global_worst'
_THIS_DIR = os.path.dirname(__file__)
files_info_filepath = os.path.join(_THIS_DIR,
                                   'ncfiles_types_nfields_nbytes.pkl')

import decoupler_shared
text_basepath = os.path.join(os.path.dirname(decoupler_shared.__file__),
                             'compression', 'worst-case', 'frozen')
glm_gpcs_mass = os.path.join(
    text_basepath,
    'mass-compression-sprint7-worst-case-glm.txt')

with open(glm_gpcs_mass) as fo:
    file_paths = [line.split()[8] for line in fo.readlines()]

# e.g. moose:/adhoc/projects/hpcdecoupler/compression/sprint-7/worst-case-glm/20151021T0000Z-000-level_1-01_03220_128.nc
# --> "20151021T0000Z-168-level_1-10_20028_0.nc"
file_names = [os.path.basename(path) for path in file_paths]

#
# TEMPORARY : restrict for speed
#
#file_names = file_names[:400]

#extract file chunk numbers.
#   e.g. "20151021T0000Z-168-level_1-10_20028_0.nc"
#   --> 168
file_chunks = [int(name.split('-')[1]) for name in file_names]

#extract file phenoms....
#   e.g. "20151021T0000Z-168-level_1-10_20028_0.nc"
#   --> 10_20028_0.nc"
file_phenom_strs = [name.split('-')[-1] for name in file_names]
#   e.g. "10_20028_0.nc"
#   --> "10_20028_0")
file_phenom_strs = [name.split('.')[0] for name in file_phenom_strs]
#   e.g. "10_20028_0.nc"
#   --> (10,20028,0)
files_m_si_lbproc = [code.split('_') for code in file_phenom_strs]
files_m_s_i_lbproc = [(int(code[0]),
                       int(code[1]) // 1000,
                       int(code[1]) % 1000,
                       int(code[2]))
                      for code in files_m_si_lbproc]

# Identify all the phenomena.
all_phenoms = sorted(set(files_m_s_i_lbproc))

# For each phenomenon in turn; for each output netcdf file: ...
# relate the number of fields to the size of each file
# NOTE: cache this key info, as it takes a few minutes...
print 'scanning file n-fields and sizes...'
if os.path.exists(files_info_filepath):
    with open(files_info_filepath) as fo:
        file_dtypes_nfields_nbytes = pickle.load(fo)
else:
#    os.remove(files_info_filepath)
#if 1:
#    select_phenoms = (
#        (10, 20, 29, 0),
#        (10, 20, 49, 0),
#        (1, 15, 201, 0),
#        (1, 3, 250, 0)
#        )
    file_dtypes_nfields_nbytes = {}
    for i_file, (filename, phenom) in enumerate(
            zip(file_names, files_m_s_i_lbproc)):
#        if phenom not in select_phenoms:
#            continue
        print '{:05} {}'.format(i_file, filename)
        filepath = os.path.join(data_dir, filename)
        ds = nc.Dataset(filepath)
        nm, ns, ni, lbproc = phenom
        msi_str = 'm{:02d}s{:02d}i{:03d}'.format(nm, ns, ni)
        main_var, = [var
                     for var in ds.variables.itervalues()
                     if (hasattr(var, 'um_stash_source') and 
                         var.um_stash_source == msi_str)]
        assert main_var.dimensions[-2:] == ('latitude', 'longitude')
        n_fields = np.prod(main_var.shape[:-2])
        n_bytes = os.stat(filepath).st_size
        dtype = main_var.dtype
        file_dtypes_nfields_nbytes[filename] = (dtype, n_fields, n_bytes)
    with open(files_info_filepath, 'wb') as fo:
        pickle.dump(file_dtypes_nfields_nbytes, fo)


print
print 'collating all phenoms x n-fields --> sizes  **EXCEPT**t=000 ...'
phenom_nfield_sizes = {}
for i_file, (filename, phenom, chunk) in enumerate(
        zip(file_names, files_m_s_i_lbproc, file_chunks)):
    if chunk == 0:
        # Skip chunk-000 for now, to avoid files with orography fields
        continue
    dtype, n_fields, n_bytes = file_dtypes_nfields_nbytes[filename]
    nfield_sizes = phenom_nfield_sizes.get(phenom, {})
    nfield_sizes_list = nfield_sizes.get(n_fields, [])
    nfield_sizes_list += [n_bytes]
    nfield_sizes[n_fields] = nfield_sizes_list
    phenom_nfield_sizes[phenom] = nfield_sizes

# Checked that there is only one size in bytes for a given nfields, and change
# all the values from nf:list[nb] to nf:nb
for phenom, nfield_sizes in phenom_nfield_sizes.iteritems():
    msg = 'Phenom {} has multiple sizes for n-fields={} : {}'
    for n_fields in sorted(nfield_sizes.keys()):
        sizes = set(nfield_sizes[n_fields])
        if len(sizes) > 1:
            raise ValueError(msg.format(phenom, n_fields, sizes))
        one_size, = list(sizes)
        phenom_nfield_sizes[phenom][n_fields] = one_size


# Work out how many have multiple n-fields value.
print
print 'numbers of different n-fields values:'
n_nfields_phenoms = {}
for phenom, nf_nb in phenom_nfield_sizes.iteritems():
    n_nfields = len(nf_nb)
    phenoms = n_nfields_phenoms.get(n_nfields, []) + [phenom]
    n_nfields_phenoms[n_nfields] = phenoms

# Show summary of number-of-different-nfields
max_nfields_values = max(n_nfields_phenoms.keys())
n_all_phenoms = len(all_phenoms)
msg = 'N phenoms with {:02d} different n-fields values : {} / {}'
for n_occurs in range(1, max_nfields_values+1):
    n_phenoms = len(n_nfields_phenoms[n_occurs])
    print msg.format(n_occurs, n_phenoms, n_all_phenoms)

## Investigate the most complex ones only...
#import matplotlib.pyplot as plt
#for phenom in all_phenoms:  #n_nfields_phenoms[max_nfields_values]:
#    if not phenom in phenom_nfield_sizes:
#        print '?missing? ', phenom
#        continue
#    nf_nbs = phenom_nfield_sizes[phenom]
#    fields_ns = sorted(nf_nbs.keys())
#    n_bytes = [list(nf_nbs[nf])[0] for nf in fields_ns]
#    plt.plot(fields_ns, n_bytes, 'x-')
#plt.show()

# Investigate the most complex ones only...
# Deduce for each phenomenon a size scale+offset, and print them
max_err = 0.0
all_normal_slopes = []
all_normal_offsets = []
typical_slope = 7.e6
typical_offset = 21.e3
max_slope, min_slope = 0.0, 1.e24
max_offset, min_offset = 0.0, 1.e24
for n_nfields in range(max_nfields_values, 1, -1):
    print
    print 'n-nfields = ', n_nfields
    for phenom in n_nfields_phenoms[n_nfields]:
        nfields_sizes = phenom_nfield_sizes[phenom]
        print
        print 'Phenom {}:'.format(phenom)
        nfs = np.array(sorted(nfields_sizes.keys()))
        nbs = np.array([nfields_sizes[nf] for nf in nfs], dtype=float)
        slope, offset, rval, pval, err = linregress(nfs, nbs)
        if np.abs(slope / typical_slope - 1.0) > 0.2:
            print
            print '  !!!!!!!!!!!!!!!'
            print '    At {}, exceptional slope = {}'.format(phenom, slope)
            print '  !!!!!!!!!!!!!!!'
            print
        elif np.abs(offset / typical_offset - 1.0) > 1.0:
            print
            print '  !!!!!!!!!!!!!!!'
            print '    At {}, exceptional offset = {}'.format(phenom, offset)
            print '  !!!!!!!!!!!!!!!'
            print
        else:
            all_normal_slopes.append(slope)
            max_slope = max(max_slope, slope)
            min_slope = min(min_slope, slope)
            all_normal_offsets.append(offset)
            max_offset = max(max_offset, offset)
            min_offset = min(min_offset, offset)
        print '  =~= {} + nf * {}   [~{:10.4g}]'.format(offset, slope, slope)
        if n_nfields > 2:
            nbs_calc = offset + slope * nfs
            nbs_errs = nbs_calc - nbs
            print '  nf:actuals = {}'.format(zip(nfs, nbs))
            print '  nf:calcs   = {}'.format(zip(nfs, nbs_calc))
            print '  nf:errors  = {}'.format(zip(nfs, nbs_errs))
            max_err_this = np.max(np.abs(nbs_errs))
            if max_err_this > max_err:
                max_err = max_err_this
                max_err_for = phenom
print
print
mean_slope = np.mean(all_normal_slopes)
# NOTE: mean_slope is effectively calculated for float32 (i.e. 4-byte) values.
# we don't really control this, but it works because the vast majority are, and the other values fail the "normal" test.
print 'nBytes/field : mean = {}'.format(mean_slope)
print '   min={:10.1f}  = mean - {:.1f}  ~ {:-9.3f}%'.format(
    min_slope,
    mean_slope - min_slope,
    100.0 * (min_slope / mean_slope - 1.0))
print '   max={:10.1f}  = mean + {:.1f}  ~ {:-9.3f}%'.format(
    max_slope,
    max_slope - mean_slope,
    100.0 * (max_slope / mean_slope - 1.0))

print
mean_offset = np.mean(all_normal_offsets)
print 'nBytes OFFSET : mean = {}'.format(mean_offset)
print '   min={:10.1f}  = mean - {:.1f}  ~ {:-9.3f} %-bytes/field'.format(
    min_offset,
    mean_offset - min_offset,
    100.0 * (min_offset - mean_offset) / mean_slope)
print '   max={:10.1f}  = mean + {:.1f}  ~ {:-9.3f} %-bytes/field'.format(
    max_offset,
    max_offset - mean_offset,
    100.0 * (max_offset - mean_offset) / mean_slope)
print
print ' Max absolute error = {:.2f} for {}'.format(max_err, max_err_for)
print '    ~= {:7.3} % of mean bytes/field'.format(100.0 * max_err / mean_slope)

#
# Now compare all (except 000s, still) to average estimates
#
gig_bytes = 2.0**30
def gsize(nbytes):
    return '({:07.3f}Gb)'.format(nbytes / gig_bytes)

print
print 'All sizes against average estimates...'
max_error = 0.0
max_at = None
for n_nfields in range(max_nfields_values, 0, -1):
    print
    print 'All phenomena with {} different n-fields:'.format(n_nfields)
    phenoms = [phenom
               for phenom, nf_nb in phenom_nfield_sizes.iteritems()
               if len(nf_nb) == n_nfields]
    phenoms = sorted(phenoms)
    n_phenom_files = len(phenoms)
    for phenom in phenoms:
        # Scan all files for this phenomenon
        chunks_filenames = {chunk:filename
                            for filename, msil, chunk in zip(file_names,
                                                             files_m_s_i_lbproc,
                                                             file_chunks)
                            if msil == phenom and chunk != 0}
        filenames_chunks = [(chunks_filenames[chunk], chunk)
                            for chunk in sorted(chunks_filenames.keys())]
        max_error_phenom = 0
        total_error_phenom = 0
        total_bytes_phenom = 0
        for filename, chunk in filenames_chunks:
            dtype, n_fields, n_bytes = file_dtypes_nfields_nbytes[filename]
            total_bytes_phenom += n_bytes
            phenom_slope = mean_slope * (dtype.itemsize / 4.0)
            n_bytes_estd = mean_offset + phenom_slope * n_fields
            error = n_bytes_estd - n_bytes
            abs_error = abs(error)
            if abs_error > max_error_phenom:
                max_error_phenom = abs_error
                max_error_at = chunk
            total_error_phenom += error
        # Report total and worst-per-file error for this phenomenon
        msg = ('Phenom {:20s}: total-size={:12d}{}  '
               'total error={:-12d}{} ~{:+6.2f}%tot,  '
               'worst per-file={:-12d}{} at chunk {}')
        n_total_error_phenom = int(np.round(total_error_phenom))
        n_max_error_phenom = int(np.round(max_error_phenom))
        print msg.format(str(phenom).rjust(20),
                         total_bytes_phenom, gsize(total_bytes_phenom),
                         n_total_error_phenom, gsize(n_total_error_phenom),
                         100.0 * total_error_phenom / total_bytes_phenom,
                         n_max_error_phenom, gsize(n_max_error_phenom),
                         max_error_at)
        if max_error_phenom > max_error:
            max_error = max_error_phenom
            max_at = phenom

print
print 'Worst-case absolute size error per phenomenon'
print '  occurs at: {}'.format(phenom)
print '  = {:12d}{} ~{:6.2f}%-field-size'.format(
    int(max_error), gsize(max_error), max_error / mean_slope)

# Calculate total difference between actual and calculated file sizes.
actual_total_size = 0
estd_total_size = 0.0
for filename, (dtype, nf, nb) in file_dtypes_nfields_nbytes.items():
    actual_total_size += nb
    phenom_slope = mean_slope * (dtype.itemsize / 4.0)
    n_bytes_estd = mean_offset + phenom_slope * nf
    estd_total_size += n_bytes_estd

print
print 'Total size of all file sizes  (excluding chunk-000)'
print 'ACTUAL    = {:12d}{}'.format(
    actual_total_size, gsize(actual_total_size))
print 'ESTIMATED = {:12d}{}'.format(
    int(estd_total_size), gsize(estd_total_size))
total_error = estd_total_size - actual_total_size
print 'Difference (over-estimate) = {}{} ~{:-6.2f}%tot'.format(
    total_error, gsize(total_error), 100.0 * total_error / actual_total_size)


# Double-check the specific timestep files in the output against expectation,
# due to previous results being based on an obsolete bugged version of
# 'gpcs_pattern_analysis.identify_timestep_versions'.
from decoupler_shared.gpcs_pattern_analysis import load_pa, load_pb
patterns_and_codes_pa = load_pa()
patterns_and_codes_pb = load_pb()
all_patterns = set(patterns_and_codes_pa.keys()) | set(patterns_and_codes_pb.keys())
patterns_and_codes = {
    patt: (patterns_and_codes_pa.get(patt, set()) |
           patterns_and_codes_pb.get(patt, set()))
    for patt in all_patterns}

_FILENAME_ELEMENTS = file_names[0].split('-')
#e.g. ['20151021T0000Z', '000', 'level_1', '01_00002_0.nc']
_FILENAME_TIMESTR = _FILENAME_ELEMENTS[0]
_FILENAME_LEVELSTR = _FILENAME_ELEMENTS[2]
_FORMAT_FILENAME_CHUNK = '{chunk:03d}'
_FORMAT_FILENAME_PHENOM = '{m:02d}_{si:05d}_{lbproc:d}'
_FORMAT_FILENAME = '{}-{}-{}-{}.nc'.format(
    _FILENAME_TIMESTR,
    _FORMAT_FILENAME_CHUNK,
    _FILENAME_LEVELSTR,
    _FORMAT_FILENAME_PHENOM
    )

def phenom_timechunk_filename(code, chunk):
    nm, nsi, lbproc = code
    return _FORMAT_FILENAME.format(chunk=chunk,
                                   m=nm, si=nsi, lbproc=lbproc)

#Quick test !
assert phenom_timechunk_filename((1, 0002, 0), 000) == file_names[0]

files_wanted = []
for pattern, codes in patterns_and_codes.iteritems():
    for i_time in range(len(pattern)):
        if pattern[i_time] == 'x':
            chunk = i_time * 3
            for code in codes:
                filename = phenom_timechunk_filename(code, chunk)
                if filename not in file_names:
                    print 'Missing : {} : {} at {}'.format(filename, code, chunk)
                files_wanted.append(filename)

# Check all wanted files were actually present.
assert set(file_names) == set(files_wanted)
