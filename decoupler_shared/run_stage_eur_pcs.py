import os
import sys
import subprocess
from decoupler.eur_frozen_pcs import LEVEL1

environ = os.environ.copy()

environ['EUR_STAGE_OUTPUT_DIR'] = os.path.join(os.environ['DATADIR'], 
                                               'decoupler', 'eur_frozen_pcs')
environ['EUR_STAGE_INPUT_DIR'] =  os.path.join(os.environ['DATADIR'], 
                                               'decoupler', 'test_data', 'eur')
environ['PYTHONPATH'] = os.path.expanduser(os.path.join('~', 'stage'))

for chunk in (0, 6):
    environ['STAGE_CURRENT_CHUNK'] = '{:03d}'.format(chunk)
    for diagnostic in LEVEL1:
        prefix = 'eur_frozen_pcs-{:03d}'.format(chunk)
        cmd = [sys.executable, '-m', 'bgd.framework', 
               'decoupler.eur_frozen_pcs', '-k', diagnostic, '-p', prefix]
        print chunk, diagnostic
        subprocess.check_output(cmd, env=environ, stderr=subprocess.STDOUT)

