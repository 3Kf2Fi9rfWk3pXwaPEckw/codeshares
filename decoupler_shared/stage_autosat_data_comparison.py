#!/bin/python
'''
----------------------------------------------------------------------

   Simple test of NetCDF data agreement with source fields-files.

----------------------------------------------------------------------
'''
import iris
import warnings
import glob
import os
import argparse
from fieldsfile_comparison_functions import *

#  Set iris switch to enable new behaviour concerning reference surfaces and
#  dimensionless vertical coordinates.
iris.FUTURE.netcdf_promote = True

#  Flag for printing more output; make true if more details are required in
#  event of unmatching results.
nomatch_print = False

#  --------------------------------------------------------------------


def list_fieldsfiles_and_netcdf(run_time, run_id, stash_codes=None):
    '''
    List all the netcdf and fields files in the given model.
    '''
    #  Return lists of the fields files and netcdf files to be compared.
    if run_id == 'glm_stage_frozen_autosat':
        ff_path = '/critical/fpos2/cylc-run/mi-al799/share/data/' + \
            run_time + '/glm_um/'
        nc_path = '/critical/fpos2/cylc-run/mi-al799/share/data/' + \
            run_time + \
            '/decoupler/glm_frozen_autosat/glm_stage_frozen_autosat/'
        fields_files = [ff_path + '/umgl.pp8']
        #  Hours past initial time at which to check data agreement.
        test_times = [0, 3, 4, 6, 13]
    elif run_id == 'ukv_stage_frozen_autosat':
        ff_path = '/critical/fpos2/cylc-run/mi-al800/share/data/' + \
            run_time + '/ukv_um/'
        nc_path = '/critical/fpos2/cylc-run/mi-al800/share/data/' + \
            run_time + '/decoupler/ukv_frozen_autosat/*/'

        #  Hours past initial time at which to check data agreement.
        test_times = [4, 6, 12]
        fields_files = [
            ff_path + 'umqvaa_pb' + str(time_inc).zfill(3)
            for time_inc in test_times]
        fields_files = fields_files + \
            ([ff_path + 'umqvaa_pc' + str(time_inc).zfill(3)
              for time_inc in test_times])
    else:
        print 'ERROR: INVALID MODEL NAME'
        exit()
 
    #  Check fields files exist.
    missing_files = [ff for ff in fields_files if not bool(glob.glob(ff))]
    if len(missing_files) > 0:
        print 'Fields files at needed times not found ', ff_path
        print 'Missing file(s) : {}'.format(missing_files)
        exit(1)
    else:
        print 'Have found {} fields files.'.format(len(fields_files))

        
    if stash_codes:
        netcdf_files = []
        for stash in stash_codes:
            path_suffix = '/*_{}*{}_*.nc'.format(stash[4:6], stash[7:10])
            netcdf_files.extend(glob.glob(nc_path + path_suffix))
    else:
        netcdf_files = glob.glob(nc_path + '/*.nc')

    if len(netcdf_files) == 0:
        print 'Have not found any netcdf files in ', nc_path
        exit(1)
    else:
        print 'Have found {} netcdf files.'.format(len(netcdf_files))

    return fields_files, netcdf_files, test_times, ff_path, nc_path


#  Takes run time and model name as inputs (e.g. 20160810T0300Z,
#  ukv_stage_frozen_autosat)
def compare_data_files(fields_files, netcdf_files,
                       test_times, tolerance, stash_codes=None):
    '''
    Carry out the data equivalence check between the netcdf and fields files.
    '''

    #  Counting variable for failures
    fail_count = 0

    ff_stash_constraint = None
    if stash_codes:
        ff_stash_constraint = iris.AttributeConstraint(
            STASH=lambda stash: stash in stash_codes)

    for fields_file in fields_files:
        #  Print current fields file name to output.
        print_current_file(result_file, str(fields_file))

        #  Catch iris file import warnings to avoid printing unimportant
        #  information to output.
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            ff_all = iris.load(fields_file, ff_stash_constraint)

        #  Loop over test times
        for ttime in test_times:
            time_constraint = iris.Constraint(
                forecast_period=in_seconds(ttime))
            for netcdf_file in netcdf_files:
                try:
                    with iris.FUTURE.context(cell_datetime_objects=True):
                        nccube_current = iris.load_cube(
                            netcdf_file, time_constraint)
                except:
                    continue

                field_name = nccube_current.name()

                #  Extract stash code of current cube.
                nc_stash = nccube_current.metadata.attributes['STASH']

                #  Check cell method for mean, max and min.
                if nccube_current.cell_methods:
                    nc_cell_method = str(nccube_current.cell_methods)
                else:
                    nc_cell_method = None

                if nomatch_print:
                    print 'NCCUBE CELL METHOD ', nc_cell_method

                #  Form iris extract constraint based upon cell method to
                #  account for stash code degeneracy.
                cell_method_constraint = get_cell_constraint(nc_cell_method)

                if nomatch_print:
                    print '-' * 50
                    print 'NetCDF STASH No. ', nc_stash, '  Field name : ', \
                        field_name, '\n'

                #  Combine stash code, time coordinate, and cell method
                #  constraints.
                diagnostic_extract_by_stash = \
                    iris.AttributeConstraint(STASH=nc_stash) & \
                    iris.Constraint(forecast_period=ttime) &\
                    cell_method_constraint

                #  Extract data cubes from the fields file that satisfy
                #  constraints to match current NetCDF cube.
                try:
                    ffcube = ff_all.extract(diagnostic_extract_by_stash)[0]
                except:
                    if nomatch_print:
                        print 'STASH code at same time, with same cell' \
                            'method, not found in fields file \n'
                    continue

                if nomatch_print:
                    print 'FIELDS FILE CELL METHOD ', ffcube.cell_methods

                #  Calculate statistics for matching fields file and NetCDF
                #  cube to compare values.
                comp_max, comp_min, comp_stdev = compare_statistics(
                    ffcube.data, nccube_current.data, tolerance)

                #  Print ouput depending upon comparison result.
                if comp_max and comp_min and comp_stdev:
                    print_comparison_success(
                        result_file, nccube_current.name(), ttime, nc_stash,
                        netcdf_file, comparison_type)
                else:
                    #  If statistics don't match, print details of the cube
                    #  for which the comparison failed.
                    fail_count += 1

                    print_comparison_fail(result_file, nccube_current.name(),
                                          ttime, nc_stash,
                                          get_cube_dim_coord_names(nccube_current),
                                          get_statistics(ffcube.data),
                                          get_statistics(nccube_current.data))
                    locate_differences(ffcube.data, nccube_current.data,
                                       'netcdf', result_file)

    print_summary(result_file, fail_count)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Compare input fieldsfile and output netCDF data.')
    parser.add_argument('run_time', metavar='run_time', type=str,
                        help='run date/time: e.g. 20160812T0000Z')
    parser.add_argument('run_id', metavar='run_id', type=str,
                        help='run id: glm_stage_frozen_autosat or '
                        'ukv_stage_frozen_autosat.')
    parser.add_argument('--tolerance', metavar='tolerance', type=float,
                        default=0.0001, help='comparison percentage error'
                        'tolerance e.g. flag differences > 0.001 per cent'
                        ' error.')
    parser.add_argument('--stash', metavar='stash', type=str, nargs='+',
                        help='test only these stash codes, e.g.'
                        'm01s00i409')
    args = parser.parse_args()

    fields_files, netcdf_files, test_times, ff_path, nc_path  = \
        list_fieldsfiles_and_netcdf(args.run_time, args.run_id, args.stash)

    if not args.stash:
        print 'Without confining the comparison to selected stash codes ' \
            'using --stash <stash code>, this tool will be slow ' \
            'to run.'

    comparison_type = 'netCDF'
    result_file = 'test_results_{}.txt'.format(args.run_id)
    print '\nResults will be output to : {}.\n'.format(result_file)
    print_header(result_file, comparison_type, args.run_id, args.run_time,
                 ff_path, nc_path)

    compare_data_files(fields_files, netcdf_files, test_times,
                       args.tolerance, args.stash)

    print 'Data comparison complete. See output: more {}'.format(result_file)
